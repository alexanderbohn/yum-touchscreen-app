//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2009 
// 
////////////////////////////////////////////////////////////////////////////////

package {

import flash.events.Event;
import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.display.StageDisplayState;

import ost.panda.genocide.map.views.MapView;

[SWF(width='1080', height='600', frameRate='60')]

public class mapalignment extends Sprite
{
	
	[Embed(source="../build/swf/countries/map_onecountry04.swf")]
	private var MapSWF:Class;
	[Embed(source="../build/swf/countries/map_onecountry04.swf",
		symbol="Russia")]
	private var JustRussiaSymbol:Class;
	
	public var imaeg:Sprite;
	public var theRussians:Sprite;
	public var mv:MapView;
	
	/// constructoor
	public function mapalignment() {
		super();
		stage.addEventListener( Event.ENTER_FRAME, initialize );
	}
	
	/// init
	private function initialize(event:Event):void {
		stage.removeEventListener( Event.ENTER_FRAME, initialize );
		trace( "mapalignment::initialize()" );
		
		stage.scaleMode = StageScaleMode.NO_SCALE;
		stage.align = StageAlign.TOP_LEFT;
		stage.displayState = StageDisplayState.FULL_SCREEN;
		
		imaeg = new MapSWF();
		//addChild(imaeg);
		
		theRussians = new JustRussiaSymbol();
		//addChild(theRussians);
		
		mv = new MapView();
		addChild(mv);
		
	}
	
}

}
