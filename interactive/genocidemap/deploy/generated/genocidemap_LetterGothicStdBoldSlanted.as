
package 
{

import mx.core.FontAsset;

[ExcludeClass]
[Embed(mimeType="application/x-font-truetype", exportSymbol="genocidemap_LetterGothicStdBoldSlanted", fontWeight="bold", _resolvedSource="/Users/fish/Dropbox/pureandapplied/preventgenocide-surface/interactive/genocidemap/build/ttf/lettergothic/LetterGothicStd-BoldSlanted.ttf", fontStyle="slanted", source="../build/ttf/lettergothic/LetterGothicStd-BoldSlanted.ttf", _file="/Users/fish/Dropbox/pureandapplied/preventgenocide-surface/interactive/genocidemap/src/genocidemap.as", fontName="LetterGothicStd-BoldSlanted-CFF", _column="2", _line="66")]

public class genocidemap_LetterGothicStdBoldSlanted extends mx.core.FontAsset 
{
    public function genocidemap_LetterGothicStdBoldSlanted() 
    { 
	    super(); 
    }

}

}
