
package 
{

import mx.core.FontAsset;

[ExcludeClass]
[Embed(mimeType="application/x-font-truetype", exportSymbol="genocidemap_LTCRemingtonTypewriterItalic", _resolvedSource="/Users/fish/Dropbox/pureandapplied/preventgenocide-surface/interactive/genocidemap/build/ttf/remington/RemingtonItalic.ttf", fontStyle="italic", source="../build/ttf/remington/RemingtonItalic.ttf", _file="/Users/fish/Dropbox/pureandapplied/preventgenocide-surface/interactive/genocidemap/src/genocidemap.as", fontName="LTCRemingtonTypewriter-Italic", _column="2", _line="45")]

public class genocidemap_LTCRemingtonTypewriterItalic extends mx.core.FontAsset 
{
    public function genocidemap_LTCRemingtonTypewriterItalic() 
    { 
	    super(); 
    }

}

}
