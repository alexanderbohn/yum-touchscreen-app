package ost.events {

import flash.events.Event;

public dynamic class OSTEvent extends Event {
	
	public static const WILL_HIDE:String = "will_hide";
	public static const HIDDEN:String = "hidden";
	public static const WILL_SHOW:String = "will_show";
	public static const SHOWN:String = "shown";
	
	public static const WILL_ACTIVATE:String = "will_deactivate";
	public static const ACTIVATED:String = "deactivated";
	public static const WILL_DEACTIVATE:String = "will_activate";
	public static const DEACTIVATED:String = "activated";
	public static const WILL_SCALE:String = "will_scale";
	public static const SCALED:String = "scaled";
	
	public static const WILL_DRAG:String = "will_drag";
	public static const DRAGGED:String = "dragged";
	public static const WILL_RELEASE_DRAG:String = "will_release_drag";
	public static const DRAG_RELEASED:String = "drag_released";
	public static const JSON_DATA_LOADED:String = "json_data_loaded";
	public static const ALL_DATA_LOADED:String = "all_data_loaded";
	public static const WAS_CLICKED:String = "wasclicked";

	public static const STOP_TIMER:String = "stoptimer";
	public static const START_TIMER:String = "starttimer";

	public var data:Object = {};
	
	public function OSTEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=true, nData:Object=null) {
		super(type, bubbles, cancelable);
		if (nData != null) data = nData;
	}
	
	override public function clone():Event {
		return new OSTEvent(type, bubbles, cancelable);
	}
}

}

