package ost.ui.stubs {

import flash.display.MovieClip;
import flash.events.Event;
import com.greensock.TweenMax;
import com.greensock.events.TweenEvent;
import ost.events.OSTEvent;

public class OSTControlStub extends MovieClip {
	
	public static const FADE_SPEED:Number = 0.5;
	
	public var ostIsTransitioning:Boolean = false;
	public var ostIsCapableOfBeingSeen:Boolean = false;
	
	public var ostIsStateTransitioning:Boolean = false;
	public var ostIsActive:Boolean = false;
	public var ostActivationLevel:Number = 0.0;
	
	public function OSTControlStub() {
		super();
	}
	
	///
	/// SHOW/HIDE
	///
	public function get transitioning():Boolean {
		return this.ostIsTransitioning;
	}
	public function hide(speed:Number = FADE_SPEED) {
		//trace(this.name+".<OSTControlStub>::hide(): transitioning = "+this.transitioning);
		if (!this.transitioning) {
			this.ostIsTransitioning = true;
			dispatchEvent(new OSTEvent(OSTEvent.WILL_HIDE));
			var tm_hide:TweenMax = new TweenMax(this, speed, { alpha: 0 });
			tm_hide.addEventListener(TweenEvent.COMPLETE, hidden);
		}
	}
	public function show(speed:Number = FADE_SPEED) {
		//trace(this.name+".<OSTControlStub>::show(): transitioning = "+this.transitioning);
		if (!this.transitioning) {
			this.ostIsTransitioning = true;
			visible = true;
			dispatchEvent(new OSTEvent(OSTEvent.WILL_SHOW));
			var tm_show:TweenMax = new TweenMax(this, speed, { alpha: 1 });
			tm_show.addEventListener(TweenEvent.COMPLETE, shown);
		}
	}
	function hidden(e:TweenEvent) {
		//trace(this.name+".<OSTControlStub>::hidden():");
		this.ostIsTransitioning = false;
		this.ostIsCapableOfBeingSeen = false;
		visible = false;
		dispatchEvent(new OSTEvent(OSTEvent.HIDDEN));
	}
	function shown(e:TweenEvent) {
		//trace(this.name+".<OSTControlStub>::shown()");
		this.ostIsTransitioning = false;
		this.ostIsCapableOfBeingSeen = true;
		dispatchEvent(new OSTEvent(OSTEvent.SHOWN));
	}
	public function toggle() {
		//trace(this.name+".<OSTControlStub>::toggle():");
		if (this.ostIsCapableOfBeingSeen == true) {
			this.hide();
		} else {
			this.show();
		}
	}
	
	///
	/// ACTIVATE/DEACTIVATE
	///
	public function get activating():Boolean {
		return this.ostIsStateTransitioning;
	}
	public function set active(ack:Boolean):void {
		//trace(this.name+".<OSTControlStub>::active<SET>():", ack);
		this.setState(ack);
	}
	public function get active():Boolean {
		return this.ostIsActive;
	}
	public function setState(whatState:Boolean = false):void {
		//trace(this.name+".<OSTControlStub>::setState():", whatState);
		if (whatState == true && !this.active) {
			this.activate();
		} else if (whatState == false && this.active) {
			this.deactivate();
		}
	}
	
	public function set activation(newActivation:Number):void {
		this.ostActivationLevel = newActivation;
	}
	public function get activation():Number {
		return this.ostActivationLevel;
	}
	public function activate(speed:Number = FADE_SPEED) {
		//trace(this.name+".<OSTControlStub>::activate(): activating = "+this.activating);
		if (!this.activating) {
			this.ostIsStateTransitioning = true;
			dispatchEvent(new OSTEvent(OSTEvent.WILL_ACTIVATE));
			var tm_a:TweenMax = new TweenMax(this, speed, { activation: 1 });
			tm_a.addEventListener(TweenEvent.COMPLETE, activated);
		}
	}
	public function deactivate(speed:Number = FADE_SPEED) {
		//trace(this.name+".<OSTControlStub>::deactivate(): activating = "+this.activating);
		if (!this.activating) {
			this.ostIsStateTransitioning = true;
			dispatchEvent(new OSTEvent(OSTEvent.WILL_DEACTIVATE));
			var tm_de:TweenMax = new TweenMax(this, speed, { activation: 0 });
			tm_de.addEventListener(TweenEvent.COMPLETE, deactivated);
		}
	}
	function activated(e:TweenEvent) {
		//trace(this.name+".<OSTControlStub>::activated()");
		this.ostIsStateTransitioning = false;
		this.ostIsActive = true;
		dispatchEvent(new OSTEvent(OSTEvent.ACTIVATED));
	}
	function deactivated(e:TweenEvent) {
		//trace(this.name+".<OSTControlStub>::deactivated():");
		this.ostIsStateTransitioning = false;
		this.ostIsActive = false;
		dispatchEvent(new OSTEvent(OSTEvent.DEACTIVATED));
	}
	public function toggleState() {
		//trace(this.name+".<OSTControlStub>::toggleState():");
		this.setState(!this.active);
	}	
}

}

