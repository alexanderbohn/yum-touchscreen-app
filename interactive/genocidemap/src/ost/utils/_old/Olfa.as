package ost.panda.genocide.map.core {

import flash.display.Sprite;
import flash.display.Loader;
import flash.display.Bitmap;
import flash.utils.*;
import flash.net.*;
import flash.text.*;
import sz.utils;

public dynamic class Olfa {
	
	public static const HORIZONTAL:uint = 0;
	public static const VERTICAL:uint = 1;
	
	
	public function Olfa() {
		super();
	}
	
	public function quickRect(Graphics g):void {
		sp.graphics.beginFill(fillColor);
        sp.graphics.drawRect(0,0,w,h);
        sp.graphics.endFill();
        sp.alpha = alpha;
        sp.x = x;
        sp.y = y;
	}
	
	public function quickRoundedRect(Graphics g):void {}
	public function quickCircle(Graphics g):void {}
	
	/// quick: horiz/vert/45(?), no alphas
	public function quickGradient(Graphics g):void {}
	public function alphaGradient(Graphics g):void {}
	public function fullGradient(Graphics g):void {}
	
	public function grid(Graphics g):void {}
	
	
}

}

