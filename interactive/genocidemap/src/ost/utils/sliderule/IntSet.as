package ost.utils.sliderule {

public class IntSet extends Object {
	
	public static const EMPTYSET:int = -1;
	public const INTSIZE:uint = 32;
	
	public var bits:Vector.<uint>;
	
	public function IntSet() {
		super();
		bits = new Vector.<uint>();
	}
	
	public function insert(element:uint):void {
		bits[int(element / INTSIZE)] |= 1 << (element % INTSIZE);
	}
	public function del(element:uint):void {
		bits[int(element / INTSIZE)] &= ~(1 << (element % INTSIZE));
	}
	public function member(element:uint):Boolean {
		return Boolean(bits[int(element / INTSIZE)] & (1 << (element % INTSIZE)));
	}
	
	public function min():uint {}
	public function max():uint {}
	public function clear():void {}
	
	public function union(s1:IntSet):void {}
	public function intersection(s1:IntSet):void {}
	public function complement():void {}
	public function subtract(s1:IntSet):void {}
	public function subSet(s1:IntSet):Boolean {}
	public function equals(s1:IntSet):Boolean {}
	public function clone():IntSet {}
	
	public function toString():String {}
	
	
}

}

