package ost.panda.genocide.touchmap.stubs {

import flash.display.MovieClip;
import flash.geom.Rectangle;
import flash.events.Event;
import flash.events.MouseEvent;
import ost.panda.genocide.touchmap.ScaleControl;

public class ScaleSliderStub extends MovieClip {
	
	public var drag:Rectangle = new Rectangle();
	
	public function ScaleSliderStub() {
		super();
		this.addEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
	}
	
	public function cleanUpYourAct(e:Event):void {
		this.removeEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
		this.addEventListener(MouseEvent.MOUSE_DOWN, theMouseIsDown);
		this.addEventListener(MouseEvent.MOUSE_UP, theMouseIsUp);
	}
	
	public function theMouseIsDown(e:MouseEvent):void {
		//trace(this.name+".<ScaleSliderStub>::theMouseIsDown()");
		e.stopPropagation();
		this.startDrag(false, drag);
	}
	public function theMouseIsUp(e:MouseEvent):void {
		//trace(this.name+".<ScaleSliderStub>::theMouseIsUp()");
		e.stopPropagation();
		this.dragMySlider(null);
		this.stopDrag();
	}
	public function dragMySlider(e:Event):void {
		if (parent) {
			(parent as ScaleControl).sliderDragged();
		}
	}
}

}

