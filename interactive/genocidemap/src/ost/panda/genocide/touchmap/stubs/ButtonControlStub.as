package ost.panda.genocide.touchmap.stubs {

import flash.events.Event;
import flash.events.MouseEvent;
import flash.filters.BitmapFilterQuality;
import flash.filters.GlowFilter;
import ost.ui.stubs.OSTControlStub;

public class ButtonControlStub extends OSTControlStub {
	
	public static const GLOW_COLOR:uint = 0x662200;
	public static const GLOW_ALPHA:Number = 0.0;
	public static const GLOW_BLUR_X:uint = 2;
	public static const GLOW_BLUR_Y:uint = 2;
	public static const GLOW_STRENGTH:Number = 1.3;
	public static const GLOW_QUALITY:Number = BitmapFilterQuality.HIGH;
	public static const GLOW_INNER:Boolean = false;
	public static const GLOW_KNOCKOUT:Boolean = false;
	public static var GLOWFILTER:GlowFilter = new GlowFilter(
		GLOW_COLOR,
		GLOW_ALPHA,
		GLOW_BLUR_X,
		GLOW_BLUR_Y,
		GLOW_STRENGTH,
		GLOW_QUALITY,
		GLOW_INNER,
		GLOW_KNOCKOUT
	);
	
	public function ButtonControlStub() {
		super();
		activation = 0.0;
		//this.setState(false);
		this.addEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
	}

	public function cleanUpYourAct(e:Event):void {
		this.removeEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
		this.addEventListener(MouseEvent.CLICK, buttonClicked);
	}
	
	public function buttonClicked(e:MouseEvent):void {
		//trace(this.name+".<ButtonControlStub>::buttonClicked():");
		e.stopPropagation();
		this.toggleState();
	}
	
	/// overrides
	public override function set activation(newActivation:Number):void {
		ostActivationLevel = newActivation;
		var gf:GlowFilter = (ButtonControlStub.GLOWFILTER.clone() as GlowFilter);
		gf.alpha = newActivation;
		this.filters = [gf];
	}
}

}

