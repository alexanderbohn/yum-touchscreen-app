package ost.panda.genocide.touchmap.stubs {

import flash.display.Shape;
import flash.display.MovieClip;
import flash.display.BlendMode;
import flash.display.Graphics;
import flash.events.Event;
import flash.events.MouseEvent;
import com.greensock.TweenMax;
import ost.events.OSTEvent;
import ost.ui.stubs.OSTControlStub;
import ost.panda.genocide.touchmap.ScaleSlider;
import ost.panda.genocide.touchmap.ScaleRule;
import ost.panda.genocide.touchmap.LargeBlackCloseButtonControl;

public class ScaleControlStub extends OSTControlStub {
	
	public static const SLIDER_MIN:Number = 1.0;
	public static const SLIDER_MAX:Number = 3.0;
	public static const SLIDER_NO_VALUE:Number = -666;
	public static const SLIDER_SNAP_SPEED = 0.2;
	
	[Inspectable(name="scaleSliderInstance",
	type=ScaleSlider, defaultValue=null)]
	public var scaleSliderInstance:ScaleSlider;
	
	[Inspectable(name="scaleRuleInstance",
	type=ScaleRule, defaultValue=null)]
	public var scaleRuleInstance:ScaleRule;
	
	[Inspectable(name="scaleSliderValue",
	type=Number, defaultValue=-1)]
	public var scaleSliderValue:Number;
	
	public var closer:LargeBlackCloseButtonControl;
	public var eraser:Shape;
	
	public function ScaleControlStub() {
		super();
		this.addEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
	}
	
	public function cleanUpYourAct(e:Event):void {
		this.removeEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
		this.addEventListener(Event.ENTER_FRAME, enterTheFrame);
		this.addEventListener(MouseEvent.MOUSE_UP, enterTheFrame);
		this.dragX = this.x + this.scaleRuleInstance.x - (this.scaleSliderInstance.width / 2);
		this.dragY = this.y + this.scaleSliderInstance.y;
		this.dragWidth = this.scaleRuleInstance.width;
		this.dragHeight = 0;
		this.scaleRuleInstance.addEventListener(MouseEvent.MOUSE_DOWN, ruleClicked);
		this.blendMode = BlendMode.LAYER;
		
		eraser = new Shape();
		addChild(eraser);
		var g:Graphics = eraser.graphics;
		g.beginFill(0x000000);
		g.drawCircle(12, 14, 26);
		g.endFill();
		eraser.blendMode = BlendMode.ERASE;
		
		closer = new LargeBlackCloseButtonControl();
		addChild(closer);
		closer.scaleX = 0.5;
		closer.scaleY = 0.5;
		closer.x = -13;
		closer.y = -14;
	}
	
	public function ruleClicked(e:MouseEvent):void {
		//trace(this.name+".<ScaleControlStub>::ruleClicked()");
		e.stopPropagation();
		this.scaleSliderInstance.stopDrag();
		this.sliderSetPixelValue(e.localX);
		this.sliderDragged();
	}
	
	public function set dragX(newX:Number):void {
		if (scaleSliderInstance) scaleSliderInstance.drag.x = newX;
	}
	public function get dragX():Number {
		if (scaleSliderInstance) return scaleSliderInstance.drag.x;
		return SLIDER_NO_VALUE;
	}
	public function set dragY(newY:Number):void {
		if (scaleSliderInstance) scaleSliderInstance.drag.y = newY;
	}
	public function get dragY():Number {
		if (scaleSliderInstance) return scaleSliderInstance.drag.y;
		return SLIDER_NO_VALUE;
	}
	public function set dragWidth(newWidth:Number):void {
		if (scaleSliderInstance) scaleSliderInstance.drag.width = newWidth;
	}
	public function get dragWidth():Number {
		if (scaleSliderInstance) return scaleSliderInstance.drag.width;
		return SLIDER_NO_VALUE;
	}
	public function set dragHeight(newHeight:Number):void {
		if (scaleSliderInstance) scaleSliderInstance.drag.height = newHeight;
	}
	public function get dragHeight():Number {
		if (scaleSliderInstance) return scaleSliderInstance.drag.height;
		return SLIDER_NO_VALUE;
	}
	
	public function enterTheFrame(e:Event):void {
		this.scaleSliderValue = ((((this.scaleSliderInstance.x + (this.scaleSliderInstance.width / 2)) - this.scaleRuleInstance.x) / this.scaleRuleInstance.width) * (SLIDER_MAX - SLIDER_MIN)) + SLIDER_MIN;
	}
	
	public function theMouseIsUp(e:Event):void {
		//trace(this.name+".<ScaleControlStub>::theMouseIsUp()");
		e.stopPropagation();
		this.scaleSliderInstance.stopDrag();
		this.sliderDragged();
	}
	
	public function sliderDragged() {
		//trace(this.name+".<ScaleControlStub>::sliderDragged(): ssix/dragX = "+scaleSliderInstance.x+"/"+this.dragX);
		dispatchEvent(new OSTEvent(OSTEvent.DRAGGED, true, true, {
			"sliderValue":this.scaleSliderValue
		}));
	}
	
	public function sliderSetPixelValue(pix:Number, doRescale:Boolean = true):void {
		//trace(this.name+".<ScaleControlStub>::sliderSetPixelValue(): "+pix);
		//this.scaleSliderInstance.x = (pix - (scaleSliderInstance.width / 2)) + this.scaleRuleInstance.x;
		var tmax = new TweenMax(this.scaleSliderInstance, SLIDER_SNAP_SPEED, {
			x: (pix - (this.scaleSliderInstance.width / 2)) + this.scaleRuleInstance.x
		});
		if (doRescale) {
			this.scaleSliderInstance.dragMySlider(null);
		}
	}
	
	public function sliderSetRelativeValue(val:Number):void {
		//trace(this.name+".<ScaleControlStub>::sliderSetRelativeValue(): "+val+"/"+this.scaleRuleInstance.width);
		val = val < SLIDER_MIN ? SLIDER_MIN : (val > SLIDER_MAX ? SLIDER_MAX : val);
		this.sliderSetPixelValue((((val - SLIDER_MIN) / (SLIDER_MAX - SLIDER_MIN)) * this.scaleRuleInstance.width), false);
	}
}

}

