package ost.panda.genocide.touchmap.stubs {

import flash.display.Shape;
import flash.display.MovieClip;
import flash.display.BlendMode;
import flash.display.Graphics;
import flash.events.Event;
import flash.events.MouseEvent;
import ost.ui.stubs.OSTControlStub;
import ost.panda.genocide.touchmap.LargeBlackCloseButtonControl;

public class HelpControlStub extends OSTControlStub {
	
	public var closer:LargeBlackCloseButtonControl;
	public var eraser:Shape;
	
	public function HelpControlStub() {
		super();
		this.addEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
	}

	public function cleanUpYourAct(e:Event):void {
		this.removeEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
		//this.addEventListener(MouseEvent.CLICK, helpClicked);
		this.blendMode = BlendMode.LAYER;
		
		eraser = new Shape();
		addChild(eraser);
		var g:Graphics = eraser.graphics;
		g.beginFill(0x000000);
		g.drawCircle(12, 8, 26);
		g.endFill();
		eraser.blendMode = BlendMode.ERASE;
		
		closer = new LargeBlackCloseButtonControl();
		addChild(closer);
		closer.scaleX = 0.5;
		closer.scaleY = 0.5;
		closer.x = -15;
		closer.y = -17;
		//closer.addEventListener(MouseEvent.CLICK, helpClicked);
	}

	public function helpClicked(e:MouseEvent):void {
		//trace(this.name+".<HelpControlStub>::helpClicked():");
		this.hide();
	}
	
}

}

