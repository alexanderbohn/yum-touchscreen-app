package ost.panda.genocide.map.controllers {

import flash.display.Sprite;
import flash.display.Shape;
import flash.display.Graphics;
import flash.display.BlendMode;
import flash.filters.BitmapFilter;
import flash.filters.BitmapFilterQuality;
import flash.filters.DropShadowFilter;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.StyleSheet;
import flash.text.TextField;
import flash.geom.Rectangle;
import sz.utils;
//import org.gif.player.GIFPlayer;
import ost.events.OSTEvent;
import ost.panda.genocide.map.core.Signatories;
import ost.panda.genocide.map.controllers.FolderController;

public class PageController extends Sprite {
	
	public static const PAGE_HEIGHT:uint = 666;
	public static const PAGE_WIDTH:uint = 412;
	public static const LEFT_PAGE_OFFSET_X:int = 33;
	public static const LEFT_PAGE_OFFSET_Y:int = -333;
	public static const RIGHT_PAGE_OFFSET_X:int = -180;
	public static const RIGHT_PAGE_OFFSET_Y:int = -333;
	public static const LEFT_PAGE_ALPHA:Number = 0.76;
	public static const RIGHT_PAGE_ALPHA:Number = 0.82;
	public static const PAGE_COLOR:uint = 0xB3E5D4;
	
	public static const SHADOW_DISTANCE:uint = 15;
	public static const SHADOW_ANGLE:uint = 55; /// degrees
	public static const SHADOW_COLOR:uint = 0x000000;
	public static const SHADOW_ALPHA:Number = 0.5;
	public static const SHADOW_BLUR_X:uint = 150;
	public static const SHADOW_BLUR_Y:uint = 99;
	public static const SHADOW_STRENGTH:Number = 0.65;
	public static const SHADOW_QUALITY:Number = BitmapFilterQuality.HIGH;
	public static const SHADOW_INNER:Boolean = false;
	public static const SHADOW_KNOCKOUT:Boolean = false;
	
	public var rightMask:Sprite;
	public var rightPage:Sprite;
	public var droppyShadow:BitmapFilter;
	public var drag:Rectangle;
	public var t:TextField = null;
	public var tb:Sprite = new Sprite();
	public var _css:StyleSheet;
	
	public function PageController() {
		//trace("PageController::__construct()");
		super();
		
		
		droppyShadow = new DropShadowFilter(
			SHADOW_DISTANCE,
			SHADOW_ANGLE,
			SHADOW_COLOR,
			SHADOW_ALPHA,
			SHADOW_BLUR_X,
			SHADOW_BLUR_Y,
			SHADOW_STRENGTH,
			SHADOW_QUALITY,
			SHADOW_INNER,
			SHADOW_KNOCKOUT
		);
		
		_css = new StyleSheet();
		_css.setStyle('name', {
			fontFamily: 				'LTCRemingtonTypewriter',
			fontWeight: 				'bold',
			color: 						"#000000",
			display: 					'inline',
			kerning: 					true,
			leading: 					17,
			letterSpacing: 				0,
			textAlign: 					"left",
			fontSize: 					38
		});
		_css.setStyle('signed', {
			fontFamily: 				'LetterGothicStd-Bold',
			fontWeight: 				'bold',
			color: 						"#000000",
			display: 					'inline',
			kerning: 					true,
			leading: 					10,
			letterSpacing: 				0,
			textAlign: 					"left",
			fontSize: 					14
		});
		_css.setStyle('declarations', {
			fontFamily: 				'LetterGothicStd-Bold',
			fontWeight: 				'bold',
			color: 						"#000000",
			display: 					'inline',
			kerning: 					true,
			leading: 					10,
			letterSpacing: 				0,
			textAlign: 					"left",
			fontSize: 					14
		});
		_css.setStyle('objections', {
			fontFamily: 				'LetterGothicStd-Bold',
			fontWeight: 				'bold',
			color: 						"#000000",
			display: 					'inline',
			kerning: 					true,
			leading: 					10,
			letterSpacing: 				0,
			textAlign: 					"left",
			fontSize: 					14
		});
		_css.setStyle('territory', {
			fontFamily: 				'LetterGothicStd-Bold',
			fontWeight: 				'bold',
			color: 						"#000000",
			display: 					'inline',
			kerning: 					true,
			leading: 					10,
			letterSpacing: 				0,
			textAlign: 					"left",
			fontSize: 					14
		});
		_css.setStyle('notes', {
			fontFamily: 				'LetterGothicStd-Bold',
			fontWeight: 				'bold',
			color: 						"#000000",
			display: 					'inline',
			kerning: 					true,
			leading: 					10,
			letterSpacing: 				0,
			textAlign: 					"left",
			fontSize: 					14
		});
	}
	
	public function drawAll(whichSignatory:String = null) {
		//trace("PageController::drawAll()");
		this.drawRightPage();
		rightPage.alpha = RIGHT_PAGE_ALPHA;
		rightPage.x = RIGHT_PAGE_OFFSET_Y;
		rightPage.y = RIGHT_PAGE_OFFSET_X;
		rightPage.blendMode = BlendMode.SCREEN;
		rightPage.addChild(tb);
		blendMode = BlendMode.DARKEN;
		filters = new Array(droppyShadow);
		if (whichSignatory != null) {
			Signatories.loadSignatoryText(whichSignatory, textFlowHollaBack);
		}
	}
	
	public function cleanUpYourAct(e:Event):void {
		//trace("PageController::cleanUpYourAct()");
		this.removeEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
	}
	
	public function drawRightPage() {
		//trace("PageController::drawRightPage()");
		if (rightPage) {
			this.removeChild(rightPage);
		}
		/*
		if (t) {
			t.removeEventListener(MouseEvent.MOUSE_DOWN, thisMouseIsDown);
			t.removeEventListener(MouseEvent.MOUSE_UP, thisMouseIsUp);
		}
		/*
		if (tb) {
			tb.removeEventListener(MouseEvent.MOUSE_DOWN, thisMouseIsDown);
			tb.removeEventListener(MouseEvent.MOUSE_UP, thisMouseIsUp);
		}
		*/
		if (rightPage) {
			rightPage.removeEventListener(MouseEvent.MOUSE_DOWN, thisMouseIsDown);
			rightPage.removeEventListener(MouseEvent.MOUSE_UP, thisMouseIsUp);
		}
		
		rightPage = new Sprite();
		this.addChild(rightPage);
		var g:Graphics = rightPage.graphics;
		g.clear();
		g.beginFill(PAGE_COLOR);
		g.drawRect(0, 0, PAGE_HEIGHT, PAGE_WIDTH);
		g.endFill();
		
		rightMask = new Sprite();
		var gg:Graphics = rightMask.graphics;
		gg.beginFill(0xFFFFFF);
		gg.drawRect(0, 0, (PAGE_WIDTH), (PAGE_HEIGHT));
		gg.endFill();
		
	}
	
	public function textFlowHollaBack(e:Event, xmlstr:String = null) {
		if (t) if (tb.contains(t)) tb.removeChild(t);
		if (rightPage) {
			if (xmlstr != null) {
				t = utils.textField(_css, xmlstr);
			} else {
				t = utils.textField(_css, String(e.target.data));
			}
			t.wordWrap = true;
			tb.rotation = (FolderController.ROTATE_FINAL * -1);
			t.width = PAGE_WIDTH - 33;
			tb.addChild(t);
			tb.x = (RIGHT_PAGE_OFFSET_Y*-1) - 300;
			tb.y = (RIGHT_PAGE_OFFSET_X*-1) + 210;
			
			rightMask.x += 130;
			rightMask.y += 145;
			rightMask.height -= 10;
			t.mask = rightMask;
			
			/*
			tb.addChild(rightMask);
			rightMask.width = PAGE_WIDTH;
			rightMask.height = PAGE_HEIGHT;
			rightMask.x -= 22;
			rightMask.y -= 33;
			tb.mask = rightMask;
			*/
			
			drag = t.getBounds(t.parent);
			drag.offset(((RIGHT_PAGE_OFFSET_Y*-1) - 300), ((RIGHT_PAGE_OFFSET_X*-1) + 210));
			drag.height = 0;
			if (t.height > tb.height) {
				drag.x -= (4*(tb.height) - this.height);
				drag.width += 2*(tb.height) + this.height;
				
				t.addEventListener(MouseEvent.MOUSE_DOWN, thisMouseIsDown);
				t.addEventListener(MouseEvent.MOUSE_UP, thisMouseIsUp);
				tb.addEventListener(MouseEvent.MOUSE_DOWN, thisMouseIsDown);
				tb.addEventListener(MouseEvent.MOUSE_UP, thisMouseIsUp);
				rightPage.addEventListener(MouseEvent.MOUSE_DOWN, thisMouseIsDown);
				rightPage.addEventListener(MouseEvent.MOUSE_UP, thisMouseIsUp);
				
				addEventListener(MouseEvent.MOUSE_DOWN, thisMouseIsDown);
				addEventListener(MouseEvent.MOUSE_UP, thisMouseIsUp);
			}
			
		}
	}
	public function thisMouseIsDown(e:MouseEvent):void {
		trace("PageController::theMouseIsDown()");
		e.stopPropagation();
		tb.startDrag(false, drag);
	}
	public function thisMouseIsUp(e:MouseEvent):void {
		trace("PageController::theMouseIsUp()");
		e.stopPropagation();
		tb.stopDrag();
	}
}

}

