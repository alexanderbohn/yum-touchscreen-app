package ost.panda.genocide.map.controllers {

import flash.display.Sprite;
import flash.display.Graphics;
import flash.display.GradientType;
import flash.display.SpreadMethod;
import flash.geom.Matrix;
import flash.text.StyleSheet;
import flash.text.TextField;
import sz.utils;

public dynamic class BackgroundDisplayController extends Sprite {
	
	public static const GRADIENT_SCALE_FACTOR:Number = 1.3;
	public static const GRADIENT_COLOR:uint = 0x024230;
	public static const GRADIENT_START:uint = 15;
	public static const GRADIENT_END:uint = 255;
	public static const BACKGROUND_COLOR:uint = 0xCDE3DC;
	public static const BACKGROUND_ALPHA:Number = 0.20;
	
	public function BackgroundDisplayController() {
		super();
	}
	
	public function drawGradient(gradientHeight:Number, displayWidth:Number, displayHeight:Number):void {
		//trace("BackgroundDisplayController::drawGradient()");
		var g:Graphics = this.graphics;
		var m:Matrix = new Matrix();
		g.clear();
		
		/// background tone
		g.beginFill(BACKGROUND_COLOR, BACKGROUND_ALPHA);
		g.drawRect(0, 0, displayWidth, displayHeight);
		g.endFill();
		
		/// gradient first
		m.createGradientBox(
			displayWidth,									/// width
			(gradientHeight * GRADIENT_SCALE_FACTOR),		/// height
			(Math.PI / 2),									/// angle of gradient in motherfucking radians
			0,												/// x-axis translation (offset by half the width)
			0												/// y-axis translation (offset by half the height)
		);
		g.beginGradientFill(
			GradientType.LINEAR, 							/// type
			[GRADIENT_COLOR, 0xFFFFFF], 					/// colors
			[1, 0],											/// alpha values
			[GRADIENT_START, GRADIENT_END],					/// ratio/distribution
			m,												/// the fucking matrix
			SpreadMethod.PAD								/// how to spread 'em. er.
		);
		g.drawRect(0, 0, displayWidth, gradientHeight);
		g.endFill();
	}
	
}

}

