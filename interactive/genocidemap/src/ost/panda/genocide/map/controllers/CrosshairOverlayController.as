package ost.panda.genocide.map.controllers {

import flash.display.Sprite;
import flash.display.Graphics;
import flash.display.DisplayObject;
import flash.display.BlendMode;
import flash.geom.Point;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.StyleSheet;
import flash.text.TextField;
import sz.utils;

public class CrosshairOverlayController extends Sprite {
	
	public static const CROSSHAIRS_COLOR:uint = 0xFE5C2C;
	public static const CROSSHAIRS_RULE:uint = 1;
	public static const CROSSHAIRS_BLEND:String = BlendMode.DARKEN;
	public static const CROSSHAIRS_LABEL_SIZE:uint = 10;
	public static const CROSSHAIRS_LABEL_OFFSET:uint = 8;
	
	public var _css:StyleSheet = null;
	public var _numbers:TextField = null;
	public var _numbers_relative:TextField = null;
	public var hairs:Sprite = null;
	public var relativeThing = null;
	
	public function CrosshairOverlayController() {
		super();
		this.addEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
		_css = new StyleSheet();
		_css.setStyle('numbers', {
			fontFamily: 				'LetterGothicStd',
			fontWeight: 				'medium',
			color: 						CROSSHAIRS_COLOR,
			display: 					'inline',
			leading: 					0,
			letterSpacing: 				0,
			fontSize: 					CROSSHAIRS_LABEL_SIZE
		});
	}
	
	public function cleanUpYourAct(e:Event):void {
		//trace("CrosshairOverlayController::cleanUpYourAct()");
		this.removeEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
		this.drawCrosshairs(parent.stage.stageWidth, parent.stage.stageHeight);
	}
	
	public function drawCrosshairs(theStageWidth:Number, theStageHeight:Number):void {
		//trace("CrosshairOverlayController::drawCrosshairs()");
		if (hairs != null) {
			removeChild(hairs);
		}
		hairs = new Sprite();
		this.addChild(hairs);
		var g:Graphics = hairs.graphics;
		g.clear();
		
		/// crosshair lines
		g.lineStyle(CROSSHAIRS_RULE, CROSSHAIRS_COLOR);
		g.moveTo(0, theStageHeight);
		g.lineTo((theStageWidth * 2), theStageHeight);
		g.moveTo(theStageWidth, 0);
		g.lineTo(theStageWidth, (theStageHeight * 2));
		
		/// number feedback box
		_numbers = utils.textField(_css, "<numbers>[g] XXXX, XXXX</numbers>");
		hairs.addChild(_numbers);
		_numbers.x = theStageWidth + CROSSHAIRS_LABEL_OFFSET;
		_numbers.y = theStageHeight + CROSSHAIRS_LABEL_OFFSET;
		_numbers_relative = utils.textField(_css, "<numbers>[r] XXXX, XXXX</numbers>");
		hairs.addChild(_numbers_relative);
		_numbers_relative.x = theStageWidth + CROSSHAIRS_LABEL_OFFSET;
		_numbers_relative.y = theStageHeight + CROSSHAIRS_LABEL_OFFSET - (_numbers_relative.height * 2);
		hairs.blendMode = CROSSHAIRS_BLEND;
		
		this.hide();
	}
	
	public function setCoordinateLabels(theX:Number, theY:Number, theRelativeX:Number, theRelativeY:Number):void {
		//trace("CrosshairOverlayController::setCoordinateLabels()");
		if (_numbers != null) {
			_numbers.htmlText = "<numbers>[g] " + Math.floor(theX) + ", " + Math.floor(theY) + "</numbers>";
		}
		if (_numbers_relative != null) {
			_numbers_relative.htmlText = "<numbers>[r] " + Math.floor(theRelativeX) + ", " + Math.floor(theRelativeY) + "</numbers>";
		}
	}
	
	public function setCoordinates(theX:Number, theY:Number, theRelativeX:Number=-666, theRelativeY:Number=-666):void {
		//trace("CrosshairOverlayController::setCoordinates()");
		this.x = theX - (this.width / 2);
		this.y = theY - (this.height / 2);
		setCoordinateLabels(theX, theY, theRelativeX, theRelativeY);
	}
	
	public function theMouseHasMoved(e:MouseEvent):void {
		//trace("CrosshairOverlayController::theMouseHasMoved()");
		this.x = e.stageX - (this.width / 2);
		this.y = e.stageY - (this.height / 2);
		setCoordinates(e.stageX, e.stageY);
		e.updateAfterEvent();
	}
	
	public function setCrosshairs(maus:MouseEvent):void {
		//trace("CrosshairOverlayController::setCrosshairs()");
		if (relativeThing != null) {
			var p = new Point(maus.localX, maus.localY);
			this.setCoordinates(maus.stageX, maus.stageY, (maus.localX), (maus.localY));
		} else {
			this.setCoordinates(maus.stageX, maus.stageY);
		}
		maus.updateAfterEvent();
	}
	
	public function toggle():void {
		if (this.visible == true) {
			this.hide();
		} else {
			this.show();
		}
	}
	public function show():void {
		if (relativeThing) relativeThing.addEventListener(MouseEvent.MOUSE_MOVE, setCrosshairs);
		this.visible = true;
	}
	public function hide():void {
		if (relativeThing) relativeThing.removeEventListener(MouseEvent.MOUSE_MOVE, setCrosshairs);
		this.visible = false;
	}
	
}

}

