package ost.panda.genocide.map.controllers {

import flash.display.Sprite;
import flash.display.Graphics;
import flash.display.DisplayObject;
import flash.display.BlendMode;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.StyleSheet;
import flash.text.TextField;
import sz.utils;
import ost.panda.genocide.map.controllers.BackgroundDisplayController;

public dynamic class GridSystem extends Sprite {
	
	public static const BACKGROUND:uint = 0xCDE3DC;
	public static const FOREGROUND:uint = 0xFFFFFF;
	public static const CROSSHAIRS:uint = 0xFF1919;
	public static const CROSSHAIRS_RULE:uint = 1;
	public static const CELL_RULE:uint = 2;
	public static const CELL_WIDTH:uint = 50;
	public static const CELL_HEIGHT:uint = 50;
	
	public static const CELL_LABELING:Boolean = true;
	public static const CELL_LABEL_SIZE:uint = 12;
	public static const CELL_LABEL_ALPHA:Number = 1.0;
	public static const CELL_LABEL_BLEND:String = BlendMode.MULTIPLY;
	public static const CELL_LABEL_COLOR:String = "#CC2569"; /// irritatingly, must be a string for flash 'css'
	public static const CELL_LABEL_AXIS_OFFSET:uint = 4;
	public static const CELL_LABEL_EDGE_OFFSET:uint = 4;
	
	public var bg:BackgroundDisplayController = new BackgroundDisplayController();
	public var _css:StyleSheet;
	public var _horiz:Object;
	public var _vert:Object;
	
	
	public var theRatRace:Object = {
		mX: -1,
		mY: -1
	}
	
	public function GridSystem() {
		//trace("GridSystem::__construct()");
		super();
		this.addEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
		this.addChild(bg);
		
		if (CELL_LABELING) {
			_css = new StyleSheet();
			_css.setStyle('cl', {
				fontFamily: 				'LetterGothicStd',
				fontWeight: 				'medium',
				color: 						CELL_LABEL_COLOR,
				display: 					'inline',
				leading: 					0,
				letterSpacing: 				0,
				fontSize: 					CELL_LABEL_SIZE
			});
			_horiz = [];
			_vert = [];
		}
	}
	
	public function cleanUpYourAct(e:Event):void {
		//trace("GridSystem::cleanUpYourAct(): parent.stage.stageWidth = "+parent.stage.stageWidth);
		this.removeEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
		this.drawGrid(CELL_WIDTH, CELL_HEIGHT, parent.stage.stageWidth, parent.stage.stageHeight);
	}
	
	public function drawGrid(cellWidth:Number, cellHeight:Number, gridWidth:Number, gridHeight:Number):void {
		//trace("GridSystem::drawGrid()");
		var g:Graphics = this.graphics;
		g.clear();
		removeAllLabels();
		
		/// first, bg rectangle
		g.beginFill(BACKGROUND);
		g.drawRect(0, 0, gridWidth, gridHeight);
		g.endFill();
		
		/// horiz lines
		g.lineStyle(CELL_RULE, FOREGROUND);
		for (var i = cellHeight; i < gridHeight; i += cellHeight) {
			g.moveTo(0, i);
			g.lineTo(gridWidth, i);
			labelHorizontalCell(i);
		}
		for (var j = cellWidth; j < gridWidth; j += cellWidth) {
			g.moveTo(j, 0);
			g.lineTo(j, gridHeight);
			labelVerticalCell(j);
		}
		
		/// crosshairs
		var hx:Number = Math.floor(gridWidth / 2);
		var hy:Number = Math.floor(gridHeight / 2);
		g.lineStyle(CROSSHAIRS_RULE, CROSSHAIRS);
		g.moveTo(hx, 0);
		g.lineTo(hx, gridHeight);
		g.moveTo(0, hy);
		g.lineTo(gridWidth, hy);
		
		/// backrgound display controller
		bg.drawGradient((gridHeight / 2), gridWidth, gridHeight);
	}
	
	/// sample mouse position
	public function startSampling():void {
		//trace("GridSystem::startSampling()");
		root.stage.addEventListener(MouseEvent.MOUSE_MOVE, storeSample);
	}
	public function stopSampling():void {
		//trace("GridSystem::stopSampling()");
		this.theRatRace = { mX: -1, mY: -1 };
		root.stage.removeEventListener(MouseEvent.MOUSE_MOVE, storeSample);
	}
	public function storeSample(e:MouseEvent):void {
		this.theRatRace.mX = e.stageX;
		this.theRatRace.mY = e.stageY;
		e.updateAfterEvent();
	}
	public function getMouseSample():Object {
		return this.theRatRace;
	}
	/// getter (r/o) for mouse samples
	public function get mX():Number {
		return Number(this.theRatRace.mX);
	}
	public function get mY():Number {
		return Number(this.theRatRace.mY);
	}
	
	
	/// cell labeling functions
	/// NOTE: horizontal cell labels need a vertical position
	/// and vice versa.
	/// crazy, amirite?
	public function labelHorizontalCell(ypos:Number):void {
		////trace( "GridSystem::labelHorizontalCell()" );
		if (!CELL_LABELING) return;
		var cl:TextField;
		if (!_horiz[ypos] || _horiz.indexOf(ypos) == -1) {
			_horiz[ypos] = utils.textField(_css, "<cl>"+ypos+"</cl>");
		}
		cl = _horiz[ypos];
		this.addChild(cl);
		cl.x = CELL_LABEL_EDGE_OFFSET;
		cl.y = ypos + CELL_LABEL_AXIS_OFFSET;
		cl.alpha = CELL_LABEL_ALPHA;
		cl.blendMode = CELL_LABEL_BLEND;
	}
	public function labelVerticalCell(xpos:Number):void {
		////trace( "GridSystem::labelVerticalCell()" );
		if (!CELL_LABELING) return;
		var cl:TextField;
		if (!_vert[xpos] || _horiz.indexOf(xpos) == -1) {
			_vert[xpos] = utils.textField(_css, "<cl>"+xpos+"</cl>");
		}
		cl = _vert[xpos];
		this.addChild(cl);
		cl.y = CELL_LABEL_EDGE_OFFSET;
		cl.x = xpos + CELL_LABEL_AXIS_OFFSET;
		cl.alpha = CELL_LABEL_ALPHA;
		cl.blendMode = CELL_LABEL_BLEND;
	}
	public function removeAllLabels():void {
		//trace("GridSystem::removeAllLabels()");
		if (!CELL_LABELING) return;
		var _all:Array = _horiz.concat(_vert);
		for (var cl in _all) {
			if (cl is DisplayObject) this.removeChild(cl);
		}
	}
	
}

}

