package ost.panda.genocide.map.controllers {

import flash.events.Event;
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.display.Shape;
import flash.display.MovieClip;
import flash.display.Graphics;
import flash.display.BlendMode;
import flash.filters.BitmapFilterQuality;
import flash.filters.DropShadowFilter;
import flash.geom.ColorTransform;
import flash.geom.Transform;
import flash.geom.Rectangle;
//import flash.geom.Matrix;
import flash.geom.Point;
import flash.text.StyleSheet;
import flash.text.TextField;
import sz.utils;
import com.greensock.TweenMax;
import com.greensock.OverwriteManager;
import com.greensock.events.TweenEvent;
import ost.events.OSTEvent;
import ost.panda.genocide.map.core.Signatories;
import ost.panda.genocide.map.views.MapView;

public dynamic class MapController extends Sprite {
	
	public static const BACKGROUND_WIDTH:uint = 948;
	public static const BACKGROUND_HEIGHT:uint = 576;
	public static const BACKGROUND_COLOR:uint = 0xFDFEEF;
	public static const BACKGROUND_ALPHA:Number = 0.5;
	public static const BACKGROUND_ALPHA_ZOOM:Number = 0.6;
	
	public static const MAP_SCALE_FACTOR:Number = 1.0;
	public static const MAP_SCALE_SPEED:Number = 0.5;
	
	public static const LABELS_OFFSET_X:int = -12;
	public static const LABELS_OFFSET_Y:uint = 130;
	public static const LABEL_ALPHA_SCALE_FACTOR:Number = 0.95;
	public static const LABEL_FONT_SIZE:uint = 2;
	
	public var mapGraphic:MapView;
	public var theSignatories:Signatories;
	public var bg:Shape = new Shape();
	public var startWidth:Number;
	public var startHeight:Number;
	public var mapScale:Number = MAP_SCALE_FACTOR;
	public var isRescaling:Boolean = false;
	//public var m:Matrix = null;
	public var _css:StyleSheet;
	
	public function MapController() {
		//trace("MapController::__construct()");
		super();
		
		/// typography for country labels
		_css = new StyleSheet();
		_css.setStyle('maplabel', {
			fontFamily: 				'LetterGothicStd-Bold',
			fontWeight: 				'medium',
			color: 						"#000000",
			display: 					'inline',
			kerning: 					true,
			leading: 					0,
			letterSpacing: 				0,
			textAlign: 					"center",
			fontSize: 					LABEL_FONT_SIZE
		});
		
		/// background
		var g:Graphics = bg.graphics;
		g.clear();
		g.beginFill(BACKGROUND_COLOR);
		g.drawRect(0, 0, BACKGROUND_WIDTH, BACKGROUND_HEIGHT);
		g.endFill();
		this.addChild(bg);
		bg.alpha = BACKGROUND_ALPHA;
		
		/// Map class from SWC
		mapGraphic = new MapView();
		this.addChild(mapGraphic);
	}
	
	/// setter-getters for background alpha
	public function set bgalpha(newalf:Number):void {
		bg.alpha = newalf;
	}
	public function get bgalpha():Number {
		return bg.alpha;
	}
	
	/// for scale factor
	public function rescale(scaleFactor:Number = MAP_SCALE_FACTOR):void {
		//trace("MapController::rescale(): scaleFactor = "+scaleFactor);
		if (!this.isRescaling) {
			this.isRescaling = true;
			var nX:Number, nY:Number, nW:Number, nH:Number, nBounds:Rectangle;
			var oX:Number, oY:Number, oW:Number, oH:Number;
			oX = Number(x);
			oY = Number(y);
			oW = Number(startWidth);
			oH = Number(startHeight);
			nX = (x * scaleFactor);
			nY = (y * scaleFactor);
			nW = (startWidth * scaleFactor);
			nH = (startHeight * scaleFactor);
			nBounds = new Rectangle(
				(-1*(nW - root.stage.stageWidth)),
				(-1*(nH - root.stage.stageHeight)),
				Math.abs(nW - root.stage.stageWidth),
				Math.abs(nH - root.stage.stageHeight)
			);
			if (nBounds != null && !nBounds.contains(nX, nY)) {
				nX = nBounds.topLeft.x;
				nY = nBounds.topLeft.y;
			}
			//trace("MapController::rescale(): scaleFactor,x,y,w,h = ", scaleFactor, this.x, this.y, this.width, this.height);
			var tmax = new TweenMax(this, MAP_SCALE_SPEED, {
				width: nW,
				height: nH,
				x: nX,
				y: nY,
				overwrite: OverwriteManager.AUTO
			});
			tmax.addEventListener(TweenEvent.COMPLETE, doneRescaling);
		}
	}
	public function doneRescaling(e:TweenEvent = null, scaleFactor = -666):void {
		this.isRescaling = false;
		if (scaleFactor != -666) {
			dispatchEvent(new OSTEvent(OSTEvent.SCALED, true, true, {
				"scaleValue": scaleFactor
			}));
		}
	}
	
	public function linkSignatoryMapFragments():void {
		//trace("MapController::linkSignatoryMapFragments()");
		for (var i:uint = 0; i < mapGraphic.numChildren; i++) {
			var mapFrag = mapGraphic.getChildAt(i);
			if (mapFrag is MovieClip) {
				Signatories.linkMapItem(mapFrag);
			}
		}
		Signatories.unclaimed.fixed = true;
		for each (var u in Signatories.unclaimed) {
			var ct:ColorTransform = u.transform.colorTransform;
			u.transform.colorTransform = new ColorTransform(0, 0, 0, 0.40,
				utils.range(51, 54),
				utils.range(72, 135),
				utils.range(137, 141),
			0);
		}
		for each (var s in Signatories.withLinks(true)) {
			var gt:ColorTransform = s.map.transform.colorTransform;
			var labeltxt:String = (s.alias != "null") ? s.alias : s.name;
			s.colorTran = new ColorTransform(0, 0, 0, 1,
				utils.range(132, 176),
				utils.range(148, 168),
				utils.range(104, 157),
			0);
			var fsize:Number = Math.floor(LABEL_FONT_SIZE * (Math.log(s.map.width / 4)));
			var maplabel:TextField = utils.textField(_css, "<maplabel><font align='center' size='"+((fsize > 2) ? fsize : 2)+"'>"+labeltxt.replace(/\s+/g, "\n")+"</font></maplabel>");
			mapGraphic.addChild(maplabel);
			maplabel.x = s.map.x + (Math.floor(s.map.width / 2) - Math.floor(maplabel.width / 2));
			maplabel.y = s.map.y + (Math.floor(s.map.height / 1.5) - Math.floor(maplabel.height / 2)) - 5;
			maplabel.x += Number(s.oX);
			maplabel.y += Number(s.oY);
			s.label = maplabel;
		}
	}
	
}

}

