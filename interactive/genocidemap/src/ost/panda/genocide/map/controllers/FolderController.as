package ost.panda.genocide.map.controllers {

import flash.display.Sprite;
import flash.display.Shape;
import flash.display.Graphics;
import flash.display.BlendMode;
import flash.display.GradientType;
import flash.display.SpreadMethod;
import flash.geom.Matrix;
import flash.geom.Rectangle;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.filters.BitmapFilter;
import flash.filters.BitmapFilterQuality;
import flash.filters.DropShadowFilter;
import flash.text.StyleSheet;
import flash.text.TextField;
import flash.utils.ByteArray;
import sz.utils;
import com.greensock.TweenMax;
import com.greensock.events.TweenEvent;
//import org.gif.player.GIFPlayer;
import ost.events.OSTEvent;
import ost.panda.genocide.map.controllers.PageController;
import ost.panda.genocide.map.core.Signatories;
import ost.panda.genocide.map.core.Signatory;
import ost.panda.genocide.touchmap.LargeBlackCloseButtonControl;

public class FolderController extends Sprite {
	
	/*
	[Embed(source='../../../../../../deploy/assets/common/images/ajax-loader-green.gif',
		mimeType='application/octet-stream')]
	public static var rawSpinnyGifBytes:Class;
	*/
	
	public static const FADE_SPEED:Number = 0.2;
	public static const ROTATE_SPEED:Number = 0.4;
	public static const ROTATE_INITIAL:uint = 0;
	public static const ROTATE_FINAL:uint = 90;
	
	public static const BACKGROUND_START:uint = 0xF2F5C4;
	public static const BACKGROUND_END:uint = 0xF6E07F;
	public static const GRADIENT_START:uint = 15;
	public static const GRADIENT_END:uint = 255;
	public static const FOLDER_HEIGHT:uint = 300;
	public static const FOLDER_WIDTH:uint = 500;
	public static const FOLDER_EXPANDED_HEIGHT:uint = 500;
	public static const FOLDER_INITIAL_X:uint = 390;
	public static const FOLDER_INITIAL_Y:uint = 475;
	public static const TAB_HEIGHT:uint = 50;
	public static const TAB_WIDTH:uint = 150;
	public static const TAB_ANGLE_SIZE:uint = 16;
	public static const TAB_NOTCH_SIZE:uint = 4;
	
	public static const SHADOW_DISTANCE:uint = 15;
	public static const SHADOW_ANGLE:uint = 38; /// degrees
	public static const SHADOW_COLOR:uint = 0x524A3F;
	public static const SHADOW_ALPHA:Number = 0.8;
	public static const SHADOW_BLUR_X:uint = 40;
	public static const SHADOW_BLUR_Y:uint = 35;
	public static const SHADOW_STRENGTH:Number = 0.9;
	public static const SHADOW_QUALITY:Number = BitmapFilterQuality.HIGH;
	public static const SHADOW_INNER:Boolean = false;
	public static const SHADOW_KNOCKOUT:Boolean = false;
	
	public var bg:Sprite;
	public var isCapableOfBeingSeen:Boolean = false;
	public var isTransitioning:Boolean = false;
	public var readState:Boolean = false;
	public var isStateTransitioning:Boolean = false;
	public var lastFolderHeight:Number = -1;
	public var lastSignatory:String = null;
	public var targetSignatory:String = null;
	public var folderTabLabel:String = "Treaty Signatory";
	public var currentSignatory:Signatory = null;
	public var thePages:PageController = null;
	public var closer:LargeBlackCloseButtonControl = null;
	public var eraser:Shape;
	
	public var _css:StyleSheet;
	public var _foldertab:TextField;
	
	public function FolderController() {
		super();
		_css = new StyleSheet();
		_css.setStyle('foldertab', {
			fontFamily: 				'LTCRemingtonTypewriter',
			fontWeight: 				'medium',
			color: 						"#000000",
			display: 					'inline',
			leading: 					8,
			letterSpacing: 				"0.2em",
			fontSize: 					27
		});
		this.addEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
	}
	
	public function cleanUpYourAct(e:Event):void {
		//trace("FolderController::cleanUpYourAct()");
		this.removeEventListener(Event.ADDED_TO_STAGE, cleanUpYourAct);
		this.drawFolder();
		this.x = FOLDER_INITIAL_X;
		this.y = FOLDER_INITIAL_Y;
		this.alpha = 0;
		this.blendMode = BlendMode.LAYER;
		this.thePages = new PageController();
		
		eraser = new Shape();
		addChild(eraser);
		var g:Graphics = eraser.graphics;
		g.beginFill(0x000000);
		g.drawCircle(-390, ((thePages.height / 2) + 288), 26);
		g.endFill();
		eraser.blendMode = BlendMode.ERASE;
		
		closer = new LargeBlackCloseButtonControl();
		closer.name = "folderControllerCloseButton";
		closer.scaleX = 0.5;
		closer.scaleY = 0.5;
		closer.rotation = (-1*ROTATE_FINAL);
		closer.y = (thePages.height / 2) + 288;
		closer.x = -390;
		closer.addEventListener(MouseEvent.CLICK, closerWasClicked);
	}
	
	public function closerWasClicked(e:Event):void {
		//trace("FolderController::closerWasClicked()");
		e.stopPropagation();
		Signatories.deactivateAll();
		this.exitReadState();
	}
	
	public function drawFolder(folderHeight:Number = FOLDER_HEIGHT):void {
		//trace("FolderController::drawFolder()");
		if (bg) {
			bg.removeEventListener(MouseEvent.MOUSE_DOWN, theMouseIsDown);
			bg.removeEventListener(MouseEvent.MOUSE_UP, theMouseIsUp);
			this.removeChild(bg);
		}
		if (_foldertab) if (_foldertab.parent) this.removeChild(_foldertab);
		
		bg = new Sprite();
		this.addChild(bg);
		bg.addEventListener(MouseEvent.MOUSE_DOWN, theMouseIsDown);
		bg.addEventListener(MouseEvent.MOUSE_UP, theMouseIsUp);
		var g:Graphics = bg.graphics;
		var m:Matrix = new Matrix();
		g.clear();
		
		/// tab label
		_foldertab = utils.textField(_css, "<foldertab>"+this.folderTabLabel+"</foldertab>");
		addChild(_foldertab);
		_foldertab.x = TAB_ANGLE_SIZE + (TAB_NOTCH_SIZE * 2);
		_foldertab.y = TAB_NOTCH_SIZE * 2;
		
		var tabWidth:Number = (TAB_WIDTH > _foldertab.width) ? TAB_WIDTH : (_foldertab.width + (TAB_ANGLE_SIZE * 2));
		var totalWidth:Number = (FOLDER_WIDTH > tabWidth) ? (FOLDER_WIDTH * 1.5) : (tabWidth + (TAB_ANGLE_SIZE * 3));
		var totalHeight:Number = folderHeight + TAB_HEIGHT;
		
		/// folder tab
		m.createGradientBox(
			FOLDER_WIDTH * 0.9,
			folderHeight,
			(Math.PI / 8), /// angle of gradient in motherfucking radians
			0, /// x-axis translation (offset by half the width)
			0 /// y-axis translation (offset by half the height)
		);
		g.beginGradientFill(
			GradientType.LINEAR,
			[BACKGROUND_START, BACKGROUND_END],
			[1, 1],
			[GRADIENT_START, GRADIENT_END], /// ratio/distribution
			m, /// the fucking matrix
			SpreadMethod.PAD /// how to spread 'em. er.
		);
		
		g.moveTo(0, TAB_HEIGHT);
		g.curveTo(TAB_NOTCH_SIZE, (TAB_HEIGHT), TAB_NOTCH_SIZE, (TAB_HEIGHT-TAB_NOTCH_SIZE));
		g.curveTo(TAB_ANGLE_SIZE, 0, TAB_ANGLE_SIZE+TAB_NOTCH_SIZE, 0);
		g.lineTo(tabWidth, 0);
		g.curveTo((tabWidth+TAB_NOTCH_SIZE), 0, (tabWidth+TAB_ANGLE_SIZE), (TAB_HEIGHT-TAB_NOTCH_SIZE));
		g.curveTo((tabWidth+TAB_ANGLE_SIZE), TAB_HEIGHT, (tabWidth+TAB_ANGLE_SIZE+TAB_NOTCH_SIZE), TAB_HEIGHT);
		
		/// rest of folder, widthwise
		g.lineTo(totalWidth-TAB_NOTCH_SIZE, TAB_HEIGHT);
		g.curveTo(totalWidth, TAB_HEIGHT, totalWidth, (TAB_HEIGHT+TAB_NOTCH_SIZE))
		
		/// rest of folder, short version
		g.lineTo(totalWidth, totalHeight-TAB_NOTCH_SIZE);
		g.curveTo(totalWidth, totalHeight, totalWidth-TAB_NOTCH_SIZE, totalHeight);
		g.lineTo(TAB_NOTCH_SIZE, totalHeight);
		g.curveTo(0, totalHeight, 0, (totalHeight-TAB_NOTCH_SIZE));
		g.lineTo(0, TAB_HEIGHT);
		g.endFill();
		
		/// add drop shadow to background sprite
		this.filters = [(new DropShadowFilter(
			SHADOW_DISTANCE,
			SHADOW_ANGLE,
			SHADOW_COLOR,
			SHADOW_ALPHA,
			SHADOW_BLUR_X,
			SHADOW_BLUR_Y,
			SHADOW_STRENGTH,
			SHADOW_QUALITY,
			SHADOW_INNER,
			SHADOW_KNOCKOUT
		))];
		
		/// center point fix?
		bg.x += (bg.width / 2) * -1;
		bg.y += (bg.height / 2) * -1;
		_foldertab.x += (bg.width / 2) * -1;
		_foldertab.y += (bg.height / 2) * -1;
		
		this.lastFolderHeight = folderHeight;
	}
	
	public function set folderHeight(newHeight:Number):void {
		this.drawFolder(newHeight);
	}
	public function get folderHeight():Number {
		return lastFolderHeight;
	}
	public function set tab(newTabLabel:String):void {
		this.folderTabLabel = newTabLabel;
		this.drawFolder(this.folderHeight);
	}
	public function get tab():String {
		return this.folderTabLabel;
	}
	
	/// DRAG 'N' DROP
	public function theMouseIsDown(e:MouseEvent):void {
		//trace("FolderController::theMouseIsDown()");
		e.stopPropagation();
		this.addEventListener(MouseEvent.MOUSE_MOVE, dragMyFolder);
		this.startDrag(false, new Rectangle(
			0, 0, root.stage.stageWidth, root.stage.stageHeight
		));
	}
	public function theMouseIsUp(e:MouseEvent):void {
		trace("FolderController::theMouseIsUp()");
		e.stopPropagation();
		this.removeEventListener(MouseEvent.MOUSE_MOVE, dragMyFolder);
		this.stopDrag();
	}
	public function dragMyFolder(e:MouseEvent):void {
		e.updateAfterEvent();
	}
	
	/// resize
	public function resize(newSize:Number = FolderController.FOLDER_HEIGHT, hollaback:Function = null):void {
		//trace("FolderController::resize():", this.targetSignatory);
		var tmax:TweenMax = new TweenMax(this, 0.3, { folderHeight: newSize });
		if (hollaback != null) tmax.addEventListener(TweenEvent.COMPLETE, hollaback);
	}
	
	/// show and hide
	public function hide(speed:Number = FADE_SPEED) {
		//trace("FolderController::hide():", this.targetSignatory);
		this.isTransitioning = true;
		var tmax:TweenMax = new TweenMax(this, speed, { alpha: 0 });
		tmax.addEventListener(TweenEvent.COMPLETE, hidden);
	}
	public function show(speed:Number = FADE_SPEED) {
		//trace("FolderController::show():", this.targetSignatory);
		if (!this.isStateTransitioning) {
			this.isTransitioning = true;
			this.isStateTransitioning = true;
			this.visible = true;
			var tmax:TweenMax = new TweenMax(this, speed, { alpha: 1 });
			tmax.addEventListener(TweenEvent.COMPLETE, shown);
		}
	}
	
	function hidden(e:Event) {
		//trace("FolderController::hidden():", this.targetSignatory);
		this.isTransitioning = false;
		this.isStateTransitioning = false;
		this.isCapableOfBeingSeen = false;
		this.visible = false;
	}
	function shown(e:Event) {
		//trace("FolderController::shown():", this.targetSignatory);
		this.isTransitioning = false;
		this.isCapableOfBeingSeen = true;
		this.enterReadState();
	}
	
	/// folder read-state transitional stuff
	public function toggleReadState() {
		//trace("FolderController::toggle():", this.targetSignatory);
		if (!this.isStateTransitioning) {
			if (this.readState) {
				this.exitReadState();
			} else {
				this.enterReadState(lastSignatory);
			}
		}
	}
	
	public function enterReadState(whichSignatory:String = null) {
		//trace("FolderController::enterReadState():", this.targetSignatory);
		if (whichSignatory != null) {
			this.targetSignatory = new String(whichSignatory);
		}
		if (this.targetSignatory != null) {
			var tmax:TweenMax = new TweenMax(this, ROTATE_SPEED, { rotation: ROTATE_FINAL });
			tmax.addEventListener(TweenEvent.COMPLETE, enterReadStateRotationComplete);
		}
	}
	public function exitReadState() {
		//trace("FolderController::exitReadState():", this.targetSignatory);
		if (!this.isStateTransitioning && this.readState) {
			this.readState = false;
			this.isStateTransitioning = true;
			if (this.thePages) if (this.thePages.parent) this.removeChild(this.thePages);
			if (closer) if (closer.parent) this.removeChild(closer);
			var tmax:TweenMax = new TweenMax(this, ROTATE_SPEED, { rotation: ROTATE_INITIAL });
			tmax.addEventListener(TweenEvent.COMPLETE, exitReadStateRotationComplete);
		}
	}
	
	public function enterReadStateRotationComplete(e:Event) {
		//trace("FolderController::enterReadStateRotationComplete():", this.targetSignatory);
		this.resize(FOLDER_EXPANDED_HEIGHT, this.readStateEntered);
	}
	public function exitReadStateRotationComplete(e:Event) {
		//trace("FolderController::exitReadStateRotationComplete():", this.targetSignatory);
		this.resize(FOLDER_HEIGHT, this.readStateExited);
	}
	public function readStateEntered(e:Event) {
		//trace("FolderController::readStateEntered():", this.targetSignatory);
		this.isStateTransitioning = false;
		this.readState = true;
		this.addChild(this.thePages);
		this.thePages.drawAll(targetSignatory);
		this.addChild(closer);
	}
	public function readStateExited(e:Event) {
		//trace("FolderController::readStateExited():", this.targetSignatory);
		this.hide();
	}
	
	
}

}

