package ost.panda.genocide.map.core {

import flash.events.EventDispatcher;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.display.MovieClip;
import flash.filters.BitmapFilter;
import flash.filters.BitmapFilterQuality;
import flash.filters.GlowFilter;
import flash.geom.ColorTransform;
import flash.geom.Point;
import flash.text.TextField;
import sz.utils;
import ost.events.OSTEvent;
import ost.panda.genocide.map.core.Signatories;

public class Signatory extends EventDispatcher {
	
	public static const SIGNATORY_TYPES:Vector.<String> = new Vector.<String>();
	SIGNATORY_TYPES[0] = 'Assumption';
	SIGNATORY_TYPES[1] = 'Ratification';
	SIGNATORY_TYPES[2] = 'Accession';
	SIGNATORY_TYPES[3] = 'Succession';
	
	public static const GLOW_COLOR:uint = 0x662200;
	public static const GLOW_ALPHA:Number = 1.0;
	public static const GLOW_BLUR_X:uint = 2;
	public static const GLOW_BLUR_Y:uint = 2;
	public static const GLOW_STRENGTH:Number = 1.3;
	public static const GLOW_QUALITY:Number = BitmapFilterQuality.HIGH;
	public static const GLOW_INNER:Boolean = false;
	public static const GLOW_KNOCKOUT:Boolean = false;
	public static var GLOWFILTER:GlowFilter = new GlowFilter(
		GLOW_COLOR,
		GLOW_ALPHA,
		GLOW_BLUR_X,
		GLOW_BLUR_Y,
		GLOW_STRENGTH,
		GLOW_QUALITY,
		GLOW_INNER,
		GLOW_KNOCKOUT
	);
	public static const COLOR_CLICK_MULTIPLIER_RED = 1.2;
	public static const COLOR_CLICK_MULTIPLIER_GREEN = 0.6;
	public static const COLOR_CLICK_MULTIPLIER_BLUE = 0.6;
	public static const COLOR_CLICK_MULTIPLIER_ALPHA = 1;
	
	public var id:Number;
	public var name:String;
	public var alias:String;
	public var safename:String;
	public var lcsafename:String;
	public var signDate:String;
	public var date:String;
	public var signatoryType:Number;
	public var declarations:String;
	public var objections:String;
	public var territory:String;
	public var notes:String;
	public var oX:Number = 0;
	public var oY:Number = 0;
	public var isLoaded:Boolean = false;
	public var isLinked:Boolean = false;
	public var isActive:Boolean = false;
	public var _map:MovieClip = null;
	public var _t:TextField = null;
	public var _ct:ColorTransform = null;
	public var _ctOrig:ColorTransform = null;
	
	public function Signatory() {
		//trace("Signatory::__construct()");
		super();
		this.addEventListener(OSTEvent.JSON_DATA_LOADED, continueLoading);
	}
	
	public function continueLoading(e:Event):void {
		this.removeEventListener(OSTEvent.JSON_DATA_LOADED, continueLoading);
		//trace("Signatory::continueLoading():", this.name, "("+this.safename+")");
		Signatories.loadOneSignatoryJSONBlob();
	}
	
	public function loadWithDollop(jsonDollop:Object):void {
		//trace("Signatory::loadWithDollop()");
		if (jsonDollop is Object) {
			this.id = new Number(jsonDollop.pk);
			this.name = new String(jsonDollop.fields.name);
			this.alias = new String(jsonDollop.fields.alias);
			this.safename = new String(jsonDollop.extras.safename);
			this.lcsafename = utils.downcase(safename);
			this.signDate = new String(jsonDollop.fields.signdate);
			this.date = new String(jsonDollop.fields.date);
			this.signatoryType = new Number(jsonDollop.fields.signatorytype);
			this.declarations = new String(jsonDollop.extras.safedeclarations);
			this.objections = new String(jsonDollop.extras.safeobjections);
			this.territory = new String(jsonDollop.extras.safeterritory);
			this.notes = new String(jsonDollop.extras.safenotes);
			this.oX = new Number(jsonDollop.fields.offset_x);
			this.oY = new Number(jsonDollop.fields.offset_y);
			this.isLoaded = true;
			dispatchEvent(new OSTEvent(OSTEvent.JSON_DATA_LOADED));
		}
	}
	
	public function get textDontFlow():String {
		var out:String = "<name>" + this.name + "</name>\n\n";
		if (this.signDate != "null") {
			out += "<signed>Signed on " + this.date + " \n" + SIGNATORY_TYPES[this.signatoryType] + " on " + this.signDate+ "</signed>\n\n\n\n";
		}
		out += "<declarations>" + this.declarations + "</declarations>\n\n";
		out += "<objections>" + this.objections + "</objections>\n\n";
		out += "<territory>" + this.territory + "</territory>\n\n";
		out += "<notes>" + this.notes + "</notes>";
		var sum:String = new String(this.declarations + this.objections + this.territory + this.notes) + "";
		if (sum.length < 1) {
			out += "<notes>No information available.</notes>"
		}
		return out;
	}
	
	public function set map(mapItem:MovieClip):void {
		//trace("Signatory::map set: "+mapItem.name);
		_map = mapItem;
		this.isLinked = true;
		this.map.addEventListener(MouseEvent.CLICK, mapItemClicked);
		//this.map.filters = [Signatory.GLOWFILTER.clone()];
		this.refreshColorTransform();
	}
	public function get map():MovieClip {
		return _map;
	}
	
	/// note to self: rethink this
	public function set label(t:TextField):void {
		_t = t;
		this.label.addEventListener(MouseEvent.CLICK, mapItemClicked);
	}
	public function get label():TextField {
		return _t;
	}
	
	public function refreshColorTransform():void {
		if (this.map != null && _ct != null) {
			this.map.transform.colorTransform = _ct;
		}
	}
	public function set colorTran(nt:ColorTransform):void {
		//trace("Signatory::colorTran<SET>(): colorTranOrig:", this.colorTranOrig, "_ct:", _ct, "_ctOrig:", _ctOrig);
		_ct = nt;
		if (this.colorTranOrig == null) {
			this.colorTranOrig = nt;
		}
		this.refreshColorTransform();
	}
	public function get colorTran():ColorTransform {
		if (this.map != null) {
			return this.map.transform.colorTransform;
		} else {
			return _ct;
		}
	}
	public function set colorTranOrig(nt:ColorTransform):void {
		if (nt != null) {
			_ctOrig = new ColorTransform();
			_ctOrig.concat(nt);
			//trace("Signatory::colorTranOrig<SET>(): _ctOrig:", _ctOrig);
		}
	}
	public function get colorTranOrig():ColorTransform {
		return _ctOrig;
	}
	
	public function revertColors():ColorTransform {
		if (this.colorTranOrig != null) {
			this.colorTran = this.colorTranOrig;
			this.refreshColorTransform();
			return this.colorTran;
		} else {
			/// this should never happen
			//trace('Signatory::revertColors(): TIME PARADOX colorTranOrig<GET> = NULL with', this.safename, this.isLinked ? "(linked)" : "(UNLINKED)");
			return null;
		}
	}
	public function concatColors(cct:ColorTransform):ColorTransform {
		if (cct != null) {
			this.colorTran.concat(cct);
			this.refreshColorTransform();
		}
		return this.colorTran;
	}
	
	public function set color(nc:*):void {
		//trace("Signatory::color<SET>():", nc);
		var nt:ColorTransform = new ColorTransform();
		nt.color = uint(nc);
		this.colorTran = nt;
	}
	public function get color():int {
		if (this.colorTran) {
			return int(this.colorTran.color);
		} else {
			return -1;
		}
	}
	public function get colorHex():String {
		if (this.color == -1) {
			return "0x??????";
		} else {
			return (new Number(this.color)).toString(16);
		}
	}
	
	public function mapItemClicked(e:MouseEvent):void {
		//trace("Signatory::mapItemClicked");
		var oe:OSTEvent = new OSTEvent(OSTEvent.WAS_CLICKED);
		oe.data.mapItem = new String(this.safename);
		Signatories.handleSignatoryClickEvent(oe);
	}
	
	public function set active(ack:Boolean):void {
		//trace("Signatory::active<SET>():", ack);
		this.setState(ack);
	}
	public function get active():Boolean {
		return this.isActive;
	}
	public function setState(whatState:Boolean = false):void {
		//trace("Signatory::setState():", whatState);
		if (whatState == true && !this.active) {
			this.willActivate();
		} else if (this.active) {
			this.willDeactivate();
		}
	}
	
	public function willActivate():void {
		//trace("Signatory::willActivate(): BEFORE", this.safename, "(item.colorHex = "+this.colorHex+")");
		var tinter:ColorTransform = new ColorTransform();
		tinter.redMultiplier = COLOR_CLICK_MULTIPLIER_RED;
		tinter.greenMultiplier = COLOR_CLICK_MULTIPLIER_GREEN;
		tinter.blueMultiplier = COLOR_CLICK_MULTIPLIER_BLUE;
		tinter.alphaMultiplier = COLOR_CLICK_MULTIPLIER_ALPHA;
		this.concatColors(tinter);
		this.colorTran = tinter;
		this.activated();
	}
	public function willDeactivate():void {
		this.revertColors();
		this.deactivated();
	}
	public function activated():void {
		//trace("Signatory::activated(): ON", this.safename);
		this.isActive = true;
	}
	public function deactivated():void {
		//trace("Signatory::deactivated(): OFF", this.safename);
		this.isActive = false;
	}
	
	
}

}

