package ost.panda.genocide.map.core {

import flash.events.EventDispatcher;
import flash.events.Event;
import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.net.URLLoader;
import flash.net.URLRequest;
import com.serialization.json.JSON;
import sz.utils;
import ost.events.OSTEvent;
import ost.panda.genocide.map.core.Signatory;

public dynamic class Signatories extends EventDispatcher {
	
	[Embed(source='../../../../../../build/json/signatory_all.json',
		mimeType='application/octet-stream')]
	public static var rawSignatoryJSON:Class;
	
	public static const DATA_URL:String = "http://localhost/lemkin/signatory/";
	public static const TEXTFLOW_URL:String = "http://localhost/lemkin/signatory/one/";
	public static const ONLINE = false;
	
	private static var _instance:Signatories = null;
	
	/// stuff
	public static var sigs:Vector.<Signatory> = new Vector.<Signatory>();
	public static var unclaimed:Vector.<MovieClip> = new Vector.<MovieClip>();
	public static var jsonDollop:Array;
	public static var doneLoading:Boolean = false;
	public static var jsonLoader:URLLoader = new URLLoader();
	
	public static function get instance():Signatories {
		return doInitialize();
	}
	
	public static function doInitialize():Signatories {
		//trace("Signatories::doInitialize()");
		if (_instance == null) {
			_instance = new Signatories();
		}
		return _instance;
	}
	
	public function Signatories() {
		//trace("Signatories::__construct()");
		super();
		if( _instance != null ) throw new Error("Error: Signatories already initialised.");
		if( _instance == null ) _instance = this;
	}
	
	public static function loadAllSignatories(useTheInternet:Boolean = ONLINE):void {
		if (useTheInternet) {
			var jsonRequest:URLRequest = new URLRequest();
			jsonRequest.url = DATA_URL;
			jsonLoader.load(jsonRequest);
			jsonLoader.addEventListener(Event.COMPLETE, parseSignatoryJSON);
		} else {
			loadSignatoriesFromJSON(JSON.deserialize(String(new rawSignatoryJSON())));
		}
	}
	
	public static function parseSignatoryJSON(e:Event):void {
		//trace("Signatories::parseSignatoryJSON()");
		jsonLoader.removeEventListener(Event.COMPLETE, parseSignatoryJSON);
		Signatories.loadSignatoriesFromJSON(JSON.deserialize(e.target.data) as Array);
	}
	
	public static function loadSignatoriesFromJSON(jsonData:Array):void {
		//trace("Signatories::loadSignatoriesFromJSON()");
		jsonDollop = jsonData;
		loadOneSignatoryJSONBlob();
	}
	
	public static function loadOneSignatoryJSONBlob():void {
		//trace("Signatories::loadOneSignatoryJSONBlob(): jsonDollop.length = "+jsonDollop.length);
		var thisDollop:Object = Signatories.jsonDollop.shift();
		if (thisDollop != null) {
			var sigID:uint = Signatories.sigs.length;
			Signatories.sigs[sigID] = new Signatory();
			Signatories.sigs[sigID].loadWithDollop(thisDollop);
		} else {
			Signatories.doneLoading = true;
			Signatories.sigs.fixed = true;
			_instance.dispatchEvent(new OSTEvent(OSTEvent.ALL_DATA_LOADED));
		}
	}
	
	/// key on safename for now
	public static function forName(theName:String):Signatory {
		var out:Signatory = null
		var vout:Vector.<Signatory> = Signatories.sigs.filter(function (
			s:Signatory,
			idx:int,
			v:Vector.<Signatory>) {
				return (utils.downcase(s.safename) == utils.downcase(theName));
			}
		);
		if (vout.length == 1) { out = vout[0] }
		return out;
	}
	
	public static function activateForName(theName:String):Boolean {
		return Signatories.sigs.filter(function (
			s:Signatory,
			idx:int,
			v:Vector.<Signatory>) {
				s.active = (
					(s.lcsafename == utils.downcase(theName)) &&
					s.isLinked &&
					!s.active
				);
				return s.active;
		}, { "theName": theName }).length > 0;
	}
	
	public static function deactivateAll():void {
		Signatories.sigs.forEach(function (
			s:Signatory,
			idx:int,
			v:Vector.<Signatory>) {
				s.active = false;
		});
	}
	
	public static function linkMapItem(mapItem:MovieClip):Boolean {
		//trace("Signatories::linkMapItem(): "+mapItem.name);
		for (var i:uint = 0; i < Signatories.sigs.length; i++) {
			if (Signatories.sigs[i].safename == mapItem.name) {
				Signatories.sigs[i].map = mapItem;
				return true;
				break;
			}
		}
		/// didn't get foundi
		Signatories.registerUnlinkedMapItem(mapItem);
		return false;
	}
	
	public static function withLinks(whatLinks:Boolean = true):Vector.<Signatory> {
		return Signatories.sigs.filter(function (
			s:Signatory,
			idx:int,
			v:Vector.<Signatory>) {
				return (s.isLinked == this.whatLinks);
		}, { "whatLinks": whatLinks });
	}
	
	public static function registerUnlinkedMapItem(mapItem:MovieClip):void {
		// trace("Signatories::registerUnlinkedMapItem(): "+mapItem.name);
		unclaimed.push(mapItem);
	}
	
	public static function unlinked():Vector.<MovieClip> {
		return unclaimed;
	}
	
	/// need to take signatory name as parameter
	/// this should get cleaned up
	public static function loadSignatoryText(signatoryName:String, hollaback:Function):void {
		if (ONLINE == true) {
			var out:String = null;
			var sigloader:URLLoader = new URLLoader();
			sigloader.load(new URLRequest(TEXTFLOW_URL));
			sigloader.addEventListener(Event.COMPLETE, hollaback);
		} else {
			hollaback(null, Signatories.forName(signatoryName).textDontFlow)
		}
	}
	
	public static function handleSignatoryClickEvent(e:OSTEvent):void {
		var whatHappened:Boolean = Signatories.activateForName(e.data.mapItem as String);
		//trace("Signatories::handleSignatoryClickEvent() ACTIVATE?... ", whatHappened);
		e.data.mapItemActivated = whatHappened;
		_instance.dispatchEvent(e);
	}
}

}

