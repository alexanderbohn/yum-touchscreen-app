package ost.panda.genocide.map.views {

import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.events.Event;
import flash.utils.getDefinitionByName;

public class Preloader extends MovieClip {
	
	public function Preloader() {
		super();
		//trace("\n\nPreloader::__construct()");
		stop();
		addEventListener(Event.ENTER_FRAME, onEnterFrame);
	}
	
	public function onEnterFrame(e:Event):void {
		//trace( "Preloader::onEnterframe()" );
		graphics.clear();
		if (framesLoaded == totalFrames) {
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			nextFrame();
			_init();
		} else {
			var percent:Number = (root.loaderInfo.bytesLoaded / root.loaderInfo.bytesTotal);
			//trace( "Preloader::percent = "+percent);
			graphics.beginFill(0);
			graphics.drawRect(0, (stage.stageHeight / 2 - 10), (stage.stageWidth * percent), 20);
			graphics.endFill();
		}
	}
	
	private function _init():void {
		var main_sprite:Class = Class(getDefinitionByName('genocidemap'));
		if (main_sprite) {
			var app:Object = new main_sprite();
			addChild(app as DisplayObject);
		}
	}
}

}

