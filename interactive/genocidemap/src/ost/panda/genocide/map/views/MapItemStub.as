package ost.panda.genocide.map.views {

import flash.events.Event;
import flash.display.MovieClip;

public dynamic class MapItemStub extends MovieClip {
	
	public var pinX:Number = 0;
	public var pinY:Number = 0;
	
	public function MapItemStub() {
		super();
		addEventListener(Event.ADDED_TO_STAGE, storeDetails);
	}
	
	public function storeDetails():void {
		removeEventListener(Event.ADDED_TO_STAGE, storeDetails);
		pinX = Math.floor(width / 2);
		pinY = Math.floor(height / 2);
	}
	
}

}

