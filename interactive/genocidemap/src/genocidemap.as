package {

import flash.system.Capabilities;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.KeyboardEvent;
import flash.events.TimerEvent;
import flash.display.Sprite;
import flash.display.MovieClip;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.display.StageDisplayState;
import flash.geom.Rectangle;
import flash.ui.Mouse;
import flash.ui.ContextMenu;
import flash.utils.Timer;
import flash.text.Font;
import flash.text.TextField;
import sz.utils;
import com.greensock.TweenMax;
import com.greensock.OverwriteManager;
import ost.events.OSTEvent;
import ost.panda.genocide.map.views.Preloader;
import ost.panda.genocide.map.core.Signatories;
import ost.panda.genocide.map.controllers.GridSystem;
import ost.panda.genocide.map.controllers.MapController;
import ost.panda.genocide.map.controllers.FolderController;
import ost.panda.genocide.map.controllers.BackgroundDisplayController;
import ost.panda.genocide.touchmap.ScaleControl;
import ost.panda.genocide.touchmap.ScaleButtonControl;
import ost.panda.genocide.touchmap.HelpControl;
import ost.panda.genocide.touchmap.HelpButtonControl;
import com.lia.utils.SWFProfiler;


/// SWF file settings
[SWF(width='1360', height='768', frameRate='30')]
[Frame(factoryClass='ost.panda.genocide.map.views.Preloader')]

public dynamic class genocidemap extends MovieClip {

	[Embed(source='../build/ttf/remington/Remington.ttf',
		fontName='LTCRemingtonTypewriter',
		mimeType='application/x-font-truetype')]
	public static var LTCRemingtonTypewriter:Class;
	[Embed(source='../build/ttf/remington/RemingtonItalic.ttf',
		fontName='LTCRemingtonTypewriter-Italic',
		fontStyle='italic',
		mimeType='application/x-font-truetype')]
	public static var LTCRemingtonTypewriterItalic:Class;
	[Embed(source='../build/ttf/lettergothic/LetterGothicStd.ttf',
		fontName='LetterGothicStd',
		mimeType='application/x-font-truetype')]
	public static var LetterGothicStd:Class;
	[Embed(source='../build/ttf/lettergothic/LetterGothicStd-Bold.ttf',
		fontName='LetterGothicStd-Bold',
		fontWeight='bold',
		mimeType='application/x-font-truetype')]
	public static var LetterGothicStdBold:Class;
	/*
	[Embed(source='../build/ttf/lettergothic/LetterGothicStd-Slanted.ttf',
		fontName='LetterGothicStd-Slanted',
		fontStyle='slanted',
		mimeType='application/x-font-truetype')]
	public static var LetterGothicStdSlanted:Class;
	*/
	[Embed(source='../build/ttf/lettergothic/LetterGothicStd-BoldSlanted.ttf',
		fontName='LetterGothicStd-BoldSlanted',
		fontStyle='slanted',
		fontWeight='bold',
		mimeType='application/x-font-truetype')]
	public static var LetterGothicStdBoldSlanted:Class;

	public static const BUTTON_SCALE_FACTOR:Number = 1.0;
	public static const AUTO_FULLSCREEN:Boolean = true;
	public static const USER_TIMEOUT_INTERVAL:uint = 300000; /// 5min
	
	public var idle:Timer = null;
	public var boundaries:Rectangle = null;
	public var theContextMenu:ContextMenu = null;
	public var bgrid:GridSystem = null;
	public var theMap:MapController = null;
	public var theFolder:FolderController = null;
	public var theScaleControl:ScaleControl = null;
	public var theScaleButton:ScaleButtonControl = null;
	public var theHelpButton:HelpButtonControl = null;
	public var theHelpControl:HelpControl = null;
	public var theSignatories:Signatories = null;
	
	public function genocidemap() {
		super();
		this.addEventListener(Event.ENTER_FRAME, initialize);
		this.addEventListener(Event.ADDED_TO_STAGE, listenForResizeEvents);
		idle = new Timer(USER_TIMEOUT_INTERVAL);
	}
	
	public function initialize(e:Event):void {
		this.removeEventListener(Event.ENTER_FRAME, initialize);
		//trace("\ngenocidemap::initialize()");
		stage.showDefaultContextMenu = false;
		theContextMenu = new ContextMenu();
		theContextMenu.hideBuiltInItems();
		this.contextMenu = theContextMenu;
		
		theSignatories = Signatories.instance;
		theSignatories.addEventListener(OSTEvent.ALL_DATA_LOADED, allDataLoaded);
		theSignatories.addEventListener(OSTEvent.WAS_CLICKED, doSignatory);
		Signatories.loadAllSignatories();
		
		SWFProfiler.init(stage, this);
	}
	
	public function allDataLoaded(e:OSTEvent):void {
		theSignatories.removeEventListener(OSTEvent.ALL_DATA_LOADED, allDataLoaded);
		//trace("genocidemap::allDataLoaded(): ALL DATA LOADED");
		theMap.linkSignatoryMapFragments();
		Mouse.hide();
	}
	
	public function listenForResizeEvents(e:Event):void {
		//trace("genocidemap::listenForResizeEvents(): stage.stageWidth = "+stage.stageWidth+", stage.stageHeight = "+stage.stageHeight);
		this.removeEventListener(Event.ADDED_TO_STAGE, listenForResizeEvents);
		stage.scaleMode = StageScaleMode.NO_SCALE;
		stage.align = StageAlign.TOP_LEFT;
		
		bgrid = new GridSystem();
		theMap = new MapController();
		theFolder = new FolderController();
		theScaleControl = new ScaleControl();
		theScaleButton = new ScaleButtonControl();
		theHelpControl = new HelpControl();
		theHelpButton = new HelpButtonControl();
		theScaleControl.name = "SLIDER";
		theScaleButton.name = "SCALEBUTTON";
		theHelpControl.name = "HELP";
		theHelpButton.name = "HELPBUTTON";
		
		stage.addChild(bgrid);
		stage.addChild(theMap);
		stage.addChild(theFolder);
		stage.addChild(theScaleButton);
		stage.addChild(theHelpControl);
		stage.addChild(theHelpButton);
		stage.addChild(theScaleControl); /// on top
		stage.addEventListener(Event.RESIZE, onStageResize);
		stage.addEventListener(KeyboardEvent.KEY_DOWN, doKeyPress);
		
		theScaleControl.visible = false;
		theScaleControl.addEventListener(OSTEvent.DRAGGED, scaleTheMap);
		theScaleControl.closer.addEventListener(MouseEvent.CLICK, deactivateScaler);
		theHelpControl.visible = false;
		theHelpControl.addEventListener(MouseEvent.CLICK, deactivateHelp);
		theHelpControl.closer.addEventListener(MouseEvent.CLICK, deactivateHelp);
		theScaleButton.scaleX = BUTTON_SCALE_FACTOR;
		theScaleButton.scaleY = BUTTON_SCALE_FACTOR;
		theScaleButton.addEventListener(OSTEvent.WILL_ACTIVATE, showTheScaler);
		theScaleButton.addEventListener(OSTEvent.WILL_DEACTIVATE, hideTheScaler);
		theHelpButton.scaleX = BUTTON_SCALE_FACTOR;
		theHelpButton.scaleY = BUTTON_SCALE_FACTOR;
		theHelpButton.addEventListener(OSTEvent.WILL_ACTIVATE, showTheHelp);
		theHelpButton.addEventListener(OSTEvent.WILL_DEACTIVATE, hideTheHelp);
		
		theMap.startWidth = theMap.width;
		theMap.startHeight = theMap.height;
		theMap.addEventListener(OSTEvent.SCALED, mapDidScale);
		
		if (AUTO_FULLSCREEN) {
			if (Capabilities.playerType == "Desktop") {
				/// StageDisplayState.FULL_SCREEN_INTERACTIVE is not defined when building SWFs
				stage.displayState = "fullScreenInteractive";
			} else {
				stage.displayState = StageDisplayState.FULL_SCREEN;
			}
		}
		
		returnToDefaults();
		idle.addEventListener(TimerEvent.TIMER, timeToReturn);
		root.stage.addEventListener(MouseEvent.MOUSE_MOVE, timeToWait);
		root.stage.addEventListener(MouseEvent.MOUSE_DOWN, theMouseIsDown);
		root.stage.addEventListener(MouseEvent.MOUSE_UP, theMouseIsUp);
		idle.start();
	}
	
	public function returnToDefaults():void {
		//trace("genocidemap::returnToDefaults()");
		if (theMap != null) {
			var nW:Number = bgrid.width;
			var nH:Number = Math.round((nW * bgrid.height) / bgrid.width);
			theMap.startWidth = nW;
			theMap.startHeight = nH;
			var tmax = new TweenMax(theMap, MapController.MAP_SCALE_SPEED, {
				width: nW,
				height: nH,
				x: 0,
				y: 0,
				overwrite: OverwriteManager.AUTO
			});
		}
		if (theScaleControl) {
			theScaleControl.sliderSetPixelValue(0, false);
			theScaleControl.hide();
		}
		if (theFolder) theFolder.visible = false;
		if (theScaleButton) theScaleButton.deactivate();
		boundaries = new Rectangle(
			(-1*(theMap.width - stage.stageWidth)),
			(-1*(theMap.height - stage.stageHeight)),
			Math.abs(theMap.width - stage.stageWidth),
			Math.abs(theMap.height - stage.stageHeight)
		);
		if (theHelpButton) theHelpButton.activate();
		Mouse.hide();
	}
	public function timeToReturn(e:TimerEvent):void { returnToDefaults(); }
	public function timeToWait(e:MouseEvent):void { idle.stop(); idle.reset(); idle.start(); }
	
	
	public function onStageResize(event:Event):void {
		//trace("genocidemap::onStageResize(): stageWidth = "+event.target.stageWidth);
		if (bgrid != null) {
			bgrid.drawGrid(GridSystem.CELL_WIDTH, GridSystem.CELL_HEIGHT, event.target.stageWidth, event.target.stageHeight);
		}
		/// map defaults
		returnToDefaults();
		if (theScaleControl != null) {
			theScaleControl.x = Math.floor(stage.stageWidth / 2) - Math.floor(theScaleControl.width / 2) + 8;
			theScaleControl.y = Math.floor(stage.stageHeight / 2) - Math.floor(theScaleControl.height / 2) - 13;
		}
		if (theScaleButton != null) {
			theScaleButton.x = Math.floor((stage.stageWidth * 2) / 3) - Math.floor(theScaleButton.width / 2);
			theScaleButton.y = stage.stageHeight - (theScaleButton.height + 30);
		}
		if (theHelpControl != null) {
			theHelpControl.x = Math.floor(stage.stageWidth / 2) - Math.floor(theHelpControl.width / 2);
			theHelpControl.y = 200;
		}
		if (theHelpButton != null) {
			theHelpButton.x = Math.floor(stage.stageWidth / 3) - Math.floor(theHelpButton.width / 2);
			theHelpButton.y = stage.stageHeight - (theHelpButton.height + 30);
		}
	}
	
	public function scaleTheMap(e:OSTEvent):void {
		//trace("genocidemap::scaleTheMap(): sliderValue = "+e.data.sliderValue);
		e.stopPropagation();
		theMap.rescale(Number(e.data.sliderValue));
	}
	public function mapDidScale(e:OSTEvent):void {
		//trace("genocidemap::mapDidScale(): value = "+e.data.scaleValue);
		e.stopPropagation();
		if (e.data.scaleValue) theScaleControl.sliderSetRelativeValue(e.data.scaleValue);
	}
	
	public function showTheScaler(e:Event):void {
		//trace("genocidemap::showTheScaler()");
		e.stopPropagation();
		Signatories.deactivateAll();
		theFolder.exitReadState();
		theMap.stopDrag();
		theHelpButton.deactivate();
		theScaleControl.show();
	}
	public function hideTheScaler(e:Event):void {
		//trace("genocidemap::hideTheScaler()");
		e.stopPropagation();
		theScaleControl.hide();
	}
	public function showTheHelp(e:Event):void {
		//trace("genocidemap::showTheHelp()");
		e.stopPropagation();
		Signatories.deactivateAll();
		theFolder.exitReadState();
		theMap.stopDrag();
		theScaleButton.deactivate();
		theHelpControl.show();
	}
	public function hideTheHelp(e:Event):void {
		//trace("genocidemap::hideTheHelp()");
		e.stopPropagation();
		theHelpControl.hide();
	}
	
	public function deactivateScaler(e:Event):void { theScaleButton.deactivate() }
	public function deactivateHelp(e:Event):void { theHelpButton.deactivate() }
	
	public function theMouseIsDown(e:MouseEvent):void {
		//var whatwhat:String = (e.target is TextField) ? "\""+e.target.text+"\"" : String(e.target.name);
		//trace("genocidemap::theMouseIsDown(): target: "+e.target+" ("+whatwhat+")");
		boundaries = new Rectangle(
			(-1*(theMap.width - stage.stageWidth)),
			(-1*(theMap.height - stage.stageHeight)),
			Math.abs(theMap.width - stage.stageWidth),
			Math.abs(theMap.height - stage.stageHeight)
		);
		theMap.startDrag(false, boundaries);
	}
	public function theMouseIsUp(e:MouseEvent):void {
		//trace("genocidemap::theMouseIsUp()");
		theMap.stopDrag();
	}
	
	public function toggleApplicationDisplayState():void {
		//trace("genocidemap::toggleApplicationDisplayState(): Capabilities.playerType = "+Capabilities.playerType);
		var fullscreenState:String = StageDisplayState.FULL_SCREEN;
		if (Capabilities.playerType == "Desktop") {
			fullscreenState = "fullScreenInteractive";
		}
		if (root.stage.displayState == StageDisplayState.NORMAL) {
			root.stage.displayState = fullscreenState;
		} else {
			root.stage.displayState = StageDisplayState.NORMAL;
		}
	}
	
	public function doKeyPress(e:KeyboardEvent):void {
		switch (utils.downcase(String.fromCharCode(e.charCode))) {
			case "s":
				theScaleControl.toggle();
				break;
			case "a":
				toggleApplicationDisplayState();
				break;
			case "m":
				Mouse.hide();
				break;
			case "n":
				Mouse.show();
				break;
			case "p":
				SWFProfiler.show();
				break;
			case "o":
				SWFProfiler.hide();
				break;
			case "d":
				returnToDefaults();
				break;
		}
	}
	
	public function doSignatory(e:OSTEvent):void {
		//trace("genocidemap::doSignatory(): e.data.mapItem = "+e.data.mapItem);
		e.stopImmediatePropagation();
		if (Boolean(e.data.mapItemActivated) == true) {
			if (theScaleButton.active) theScaleButton.deactivate();
			if (theHelpButton.active) theHelpButton.deactivate();
			theFolder.targetSignatory = String(e.data.mapItem);
			theFolder.show();
		} else {
			theFolder.exitReadState();
		}
	}
}

}
