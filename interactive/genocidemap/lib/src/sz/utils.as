package sz {

import flash.display.Sprite;
import flash.display.Loader;
import flash.display.Bitmap;
import flash.utils.*;
import flash.net.*;
import flash.text.*;

public class utils extends Object {
	
	public function utils()
	{
		super();
	}

	public static function upcase(lcStr:String){
		var str = "";
		for (var i = 0; i<lcStr.length; i++) {
			str += lcStr.charAt(i).toUpperCase()
		}
		return str;
	}

	public static function downcase(lcStr:String){
		var str = "";
		for (var i = 0; i<lcStr.length; i++) {
			str += lcStr.charAt(i).toLowerCase()
		}
		return str;
	}

	public static function capcase(lcStr:String){
		var str = "";
		for (var i = 0; i<lcStr.length; i++) {
			if (i==0){
				str += lcStr.charAt(i).toUpperCase()
			}else{
				str += lcStr.charAt(i).toLowerCase()
			}
		}
		return str;
	}
	
	/*
	public static function contains(obj, key){
		
		if (obj is Array){
			return (obj.indexOf(key)!=-1)
		}
		
		for (var k in obj) if (k==key) return true
		return false
	}
	*/

	/*
	public static function filledRect(fillColor, x,y, w,h, alpha=1){
		var sp = new DSprite()
		sp.graphics.beginFill(fillColor);
		sp.graphics.drawRect(0,0, w,h);
		sp.graphics.endFill();
		sp.alpha = alpha
		sp.x = x
		sp.y = y
		
		return sp
	}

	public static function roundedRect(fillColor, radius, x,y, w,h){
		var sp = new DSprite()
		sp.graphics.beginFill(fillColor);
		sp.graphics.drawRoundRect(x,y, w,h, radius,radius);
		sp.graphics.endFill();
		
		return sp
	}

	public static function loadImage(url, callback=null){
		var loader = new Loader()
		if (callback){
			loader.contentLoaderInfo.addEventListener("complete", callback)
		}
		loader.load(new URLRequest(url))
		return loader
		
	}
		
	public static function imageButton(img, hoverImg=null){
		if (img is String){
			return _imageUrlButton(img, hoverImg)
		}else if(img is Bitmap){
			return _imageBitmapButton(img, hoverImg)
		}
	}


	private static function _imageBitmapButton(img, hoverImg=null){
		var sp = new DSprite()
		sp.addChild(img)
		if (hoverImg){
			sp.addChild(hoverImg)
			hoverImg.alpha = 0
			hoverImg.visible = false
			sp.addEventListener('mouseOver',function(){
				hoverImg.alpha = 1
				hoverImg.visible = true
			})
			sp.addEventListener('mouseOut',function(){
				hoverImg.alpha = 0
				hoverImg.visible = false
			})
		}
		sp.buttonMode = true
		sp.mouseChildren = false
		return sp
	}
	
	private static function _imageUrlButton(url, hoverUrl=null)
	{
		var sp = new DSprite()
		var img = loadImage(url)
		sp.addChild(img)
		if (hoverUrl){
			var hImg = loadImage(hoverUrl)
			hImg.alpha=0
			hImg.visible=false
			sp.addChild(hImg)
			sp.addEventListener('mouseOver',function(){
				hImg.alpha = 1
				hImg.visible = true
				//img.alpha = 0
				//img.visible = false
			})
			sp.addEventListener('mouseOut',function(){
				hImg.alpha = 0
				hImg.visible = false
				//img.alpha = 1
				//img.visible = true
			})
		}
		sp.buttonMode = true
		sp.mouseChildren = false
		return sp
	}
	*/
	public static function textField(css, messg=null){
		var txt = new TextField()
		txt.styleSheet = css
		txt.embedFonts = true;
		txt.autoSize = TextFieldAutoSize.LEFT;
		txt.wordWrap = false
		txt.antiAliasType=AntiAliasType.ADVANCED
		txt.selectable = false
		if (messg) txt.htmlText = messg
		return txt
	}
	
	
	public static function textButton(css, messg='')
	{
		var sp = new DSprite()
		sp.txt = textField(css, messg)
		sp.addChild(sp.txt)
		sp.buttonMode = true
		sp.mouseChildren = false
		return sp
	}


	private static function _hasAttr(obj){
		for (var k in obj){
			//if (obj.hasOwnProperty(k)) return true
			return true
		}
		return false
	}
	
	public static function ptrace(obj){
		trace(pformat(obj, '  '))
	}	 
	
	public static function plog(label, obj){
		trace(label+": "+pformat(obj))
	}

	public static function pformat(obj, padding=''){
		if (_hasAttr(obj)){
			if (obj is Array){
				var elements = ["[ "]
				for each (var elt in obj){
					elements.push(padding+pformat(elt,padding+'	 ')+",")
				}
				var depad = padding.substr(0,Math.max(padding.length-2, 0))
				elements.push(depad+"]")
				return elements.join("\n")
				
			}else{
				var output = ["{"]
				for (var k in obj){
					output.push(padding+k+": "+pformat(obj[k],padding+'	 '))
				}
				output.push([padding.substr(2)+"}"])
				return output.join("\n")
			}
		}else{
			if (obj is String) return '"'+String(obj)+'"'
			else if (obj is Number) return String(obj)
			//else return ("<"+getQualifiedClassName(obj).match(/::(.*)/)[1] + ">")
			else return ("<"+getQualifiedClassName(obj) + ">")
		}
	}
	
	/*
	public static function rgb(r:uint, g:uint, b:uint):uint {
		return (r << 16 | g << 8 | b);
	}
	*/
	
	public static function range(lo:uint, hi:uint):uint {
		var out:uint = Math.round(Math.random() * Math.abs(hi - lo)) + lo;
		//trace("sz.utils::range(): lo/hi/out = "+lo+"/"+hi+"/"+out);
		return out;
	}

}

}

