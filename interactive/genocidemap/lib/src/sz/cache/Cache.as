package sz.cache
{
	
import flash.utils.*;
import flash.events.*;
import flash.display.*;
import flash.net.*;
import flash.utils.*;
import sz.library.*;
import com.greensock.*;
import sz.*;

	
	public class Cache extends Sprite{
        static var TIMEOUT = 3500
        
        var _len:Number // the length of _queue + _cache
        var _queue:Array
        var _cache:HashMap
        var _fetching = null // {url:"", loader:Loader, start:Date}
        
        var _defaultFade // number of seconds for fade up after load
        var _watchdog:Timer

        public function Cache(fadeTime=0){
            super()
			_defaultFade = fadeTime
            _queue = [] // {url:,img:new Loader(), loaded:false}
            _cache = new HashMap() // keyed by url {url:, img:, loaded:false}
            _watchdog = new Timer(1000/2)
            _watchdog.addEventListener("timer", _kickQueue)
            _watchdog.start()
        }
        	    

        public function fetch(url:String, attrs=null){
            if (attrs==null) attrs = {}

            // if (_cache.containsKey(url)) //trace('cache '+url)
            if (_cache.containsKey(url)){
                var img = new Bitmap(_cache[url].clone())
                var container = _newContainer(img, attrs)
                container.loaded = true
            }else{
                attrs.url = url
                var container = _newContainer(new Bitmap(), attrs)
                _enqueue(container)
            }
            return container
        }


        private function _newContainer(dispimg, attrs=null){
            var container = new DSprite()
            for (var k in attrs) container[k] = attrs[k]
            container.img = dispimg
            
            container.addChild(container.img)
            if (_defaultFade>0){
                // container.img.alpha = 0
                // if (container.img is Bitmap){
                //     TweenMax.to(container.img, _defaultFade, {autoAlpha:1})
                // } 
            }
            container.loaded = false
            return container
        }

        private function _enqueue(image){
            _queue.push(image)
            // //trace("fetch "+image.url)
            
            if (_fetching==null) _loadNext()
        }
        

        private function _loadNext(){
            if (_queue.length==0){
                // //trace('done loading')
                _fetching = null
            }else{
                var next = _queue[0]
                
                if (_cache.containsKey(next.url)){
                    // //trace("cache "+next.url)
                    next.removeChild(next.img)
                    next.img = new Bitmap(_cache[next.url].clone())
                    next.addChildAt(next.img,0)
                    next.loaded = true
                    next.dispatchEvent(new Event("fetch"))

                    _queue.shift()
                    _loadNext()
                }else{
                    // //trace("get   "+next.url)
                    _fetching = {url:next.url,
                                 loader: new Loader(),
                                 start:new Date()}
                    _fetching.loader.contentLoaderInfo.addEventListener("complete", _loadComplete)
                    _fetching.loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, _fail)
                    _fetching.loader.load(new URLRequest(_fetching.url))
                }
            }
        }
        
        private function _fail(e){
            // //trace("FAIL "+e.text)
            if (!String(e.text).match(/Load Never Completed./)) return
            //var failUrl = e.text.match(/^.*URL: (.*)$/)[1]
            _queue.push(_queue.shift())
            _loadNext()
        }
        
        private function _loadComplete(e){
            _cache[_fetching.url] = e.target.content.bitmapData.clone()
            
            for (var i=0; i<_queue.length; i++){
                var elt = _queue[i]
                if (elt.url==_fetching.url){
                    _queue.splice(i,1)
                    
                    elt.removeChild(elt.img)
                    elt.img = new Bitmap(_cache[elt.url].clone())
                    elt.addChildAt(elt.img,0)
                    elt.loaded = true
                    elt.dispatchEvent(new Event("fetch"))
                    
                    //if (_defaultFade>0) TweenMax.to(elt.img, _defaultFade, {autoAlpha:1})
                    //trace("got "+elt.url)
                    break
                }
            }

            if (_queue.length>0){
                _loadNext()
            }else{
                //trace("Done loading")
                _fetching = null
            } 
        }

        
        private function _kickQueue(e){
            if (_fetching != null){
                var loader = _fetching.loader
                var dur = new Date().time - _fetching.start.time
                // //trace("loading since "+_fetching.start+" ("+dur+")")
                // //trace("have "+_fetching.loader.contentLoaderInfo.bytesLoaded+" of "+_fetching.loader.contentLoaderInfo.bytesTotal)                
                if (dur > TIMEOUT && _fetching.loader.contentLoaderInfo.bytesTotal == 0){
                    //trace("kick")
                    _loadNext()
                }
            }
            
        }

        
	}
	
}	
	
