package sz {
import flash.events.Event;

public dynamic class DEvent extends Event {
	public static const EVENT_NAME:String = "customEvent";
	public var data;

	public function DEvent( type:String, userData=null, bubbles:Boolean=true, cancelable:Boolean=false )
	{
		super(type, bubbles, cancelable);
		data = userData
	}

	override public function clone():Event
	{
		return new DEvent(type, data, bubbles, cancelable);
	}

}
}

