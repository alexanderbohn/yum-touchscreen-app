/*
 Copyright (c) 2007 Paulius Uza  <paulius@uza.lt>
 All rights reserved.
  
 Permission is hereby granted, free of charge, to any person obtaining a copy 
 of this software and associated documentation files (the "Software"), to deal 
 in the Software without restriction, including without limitation the rights 
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is furnished 
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all 
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

@ignore
*/

package sz.library
{
	
	import flash.utils.*;
	import flash.events.*;
	
	public dynamic class Library extends Proxy implements IEventDispatcher
	{


		/**
		 *  //Library OBJECT CLASS
		 *  //EXAMPLE USAGE (in any other class):
		 * 
		 *  private var global:Global = Library.getInstance();
		 *
		 *  global.testVariableA = "hello";
		 *  global.anyClass = new Sprite();
		 *  //trace(testVariableA);
		 *
		 *  //you can aso "watch" variables:
		 *  
		 *  global.addEventListener(LibraryEvent.PROPERTY_CHANGED,onPropChanged);
		 *  global.variable = 1;
		 *  global.variable = 2;
		 * 
		 *  private function onPropChanged(e:GlobalEvent):void {
		 *		trace ("property "+ e.property + " has changed to " + global[e.property]);
		 *	} 
		 * 
		 */


        /*private static var instance:Library = null;*/
        private static var instances:Object = {};
		private static var allowInstantiation:Boolean = false;
		private var libRepository:HashMap;
		private var dispatcher:EventDispatcher;
		
		public static function getLibrary(libname='') : Library {
            var libInstance = null
		    for (var instname in instances){
		        if (instname==libname){
		            return instances[instname]
		        }
		    }
		    
			Library.allowInstantiation = true;
			Library.instances[libname] = new Library();
			Library.allowInstantiation = false;
			Library[libname] = Library.instances[libname]
			return Library.instances[libname];
	    }
		
		/**
		 * Singletonville constructor. Use <code>Library.getInstance();</code> instead.
		 */
		
		public function Library() {
			if (getQualifiedClassName(super) == "sz.library::Library" ) {
				if (!allowInstantiation) {
					throw new Error("Error: Instantiation failed: Use Library.getInstance() instead of new Library().");
				} else {
					libRepository = new HashMap();
					dispatcher = new EventDispatcher(this);
				}
			}
		}
 	 	
 	 	override flash_proxy function callProperty(methodName:*, ... args):* {
	        var result:*;
	       	switch (methodName.toString()) {
	            default:
	                result = libRepository.getValue(methodName).apply(libRepository, args);
	            break;
	        }
	        return result;
	    }
	    
 	 	override flash_proxy function deleteProperty(name:*):Boolean {
		    return libRepository.remove(name);
			dispatchEvent(new LibraryEvent(LibraryEvent.PROPERTY_CHANGED,name));
		}

 	 	override flash_proxy function getProperty(name:*):* {
		    return libRepository.getValue(name);
		}
		
		override flash_proxy function setProperty(name:*, value:*):void {
			var oldValue = libRepository.getValue(name);
			libRepository.put(name , value);
			
			if(oldValue !== value) {
				dispatchEvent(new LibraryEvent(LibraryEvent.PROPERTY_CHANGED,name));
			}
		}
		
		// like setting a property directly, but will always send the 
		// changed event, even if the new value is the same as the old
		public function updateProperty(name:*, value:*):void {
            libRepository.put(name, value);
			dispatchEvent(new LibraryEvent(LibraryEvent.PROPERTY_CHANGED,name));
		}
		
		public function get length():int {
	    	var retval:int = libRepository.size();
	    	return retval;
	    }
	    
        protected var _slots:Array; // array of object's properties
        override flash_proxy function nextNameIndex (index:int):int {
            // initial call
            if (index == 0) {
                _slots = new Array();
                for (var x:* in libRepository) {
                    _slots.push(x);
                }
            }

            if (index < _slots.length) {
                return index + 1;
            } else {
                return 0;
            }
        }

        override flash_proxy function nextName(index:int):String {
            return _slots[index - 1]
        }	    
	    
        override flash_proxy function nextValue(index:int):* {
            return libRepository.getValue(_slots[index - 1])
        }	    
	    
	    public function containsValue(value:*):Boolean{
	    	var retval:Boolean = libRepository.containsValue(value);
	   		return retval;
	    }
	    
	   	public function containsKey(name:String):Boolean{
	    	var retval:Boolean = libRepository.containsKey(name);
	   		return retval;
	    }
	    
	   	public function put(name:String, value:*):void {
	    	libRepository.put(name,value);
	    }
	    
	    public function take(name:*):* {
	    	return libRepository.getValue(name);
	    }
	    
	    public function remove(name:String):void {
	    	libRepository.remove(name);
	    }
	    
	    public function toString():String {
	    	var temp:Array = new Array();
	    	for (var key:* in libRepository) {
	    		temp.push ("{" + key + ":" + libRepository[key] + "}");
	    	}
	    	return temp.join(",");
	    }
	    
	    /**
	    *   Event Dispatcher Functions
	    */
	    
	    public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void{
        	dispatcher.addEventListener(type, listener, useCapture, priority);
	    }
	           
	    public function dispatchEvent(evt:Event):Boolean{
	        return dispatcher.dispatchEvent(evt);
	    }
	    
	    public function hasEventListener(type:String):Boolean{
	        return dispatcher.hasEventListener(type);
	    }
	    
	    public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void{
	        dispatcher.removeEventListener(type, listener, useCapture);
	    }
	                   
	    public function willTrigger(type:String):Boolean {
	        return dispatcher.willTrigger(type);
	    }
	}
}