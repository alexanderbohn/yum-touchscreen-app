//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2009 
// 
////////////////////////////////////////////////////////////////////////////////

package {
	import flash.display.Sprite;
	
	import flashx.textLayout.compose.StandardFlowComposer;
	import flashx.textLayout.container.ContainerController;
	import flashx.textLayout.elements.ParagraphElement;
	import flashx.textLayout.elements.SpanElement;
	import flashx.textLayout.elements.TextFlow;

	/** Simplest possible "Hello, World" text example */
	public class textlayoutsrule extends Sprite
	{
		public function textlayoutsrule() {
			super();
			var textFlow:TextFlow = new TextFlow();
			var p:ParagraphElement = new ParagraphElement();
			textFlow.addChild(p);
			
			var span:SpanElement = new SpanElement();
			span.text = "Hello, World";
			span.fontSize = 48;
			p.addChild(span);
			
			textFlow.flowComposer.addController(new ContainerController(this, 400, 200));
			textFlow.flowComposer.updateAllControllers();
		}
	}
}
