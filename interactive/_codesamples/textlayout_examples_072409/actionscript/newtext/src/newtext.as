//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2009 
// 
////////////////////////////////////////////////////////////////////////////////

package
{

import flash.events.Event;
import flash.display.Sprite;
import flash.utils.getDefinitionByName;
import flash.net.registerClassAlias;

import ost.newtext.views.ContextMenuText;
import ost.newtext.views.CustomImportMarkup;
import ost.newtext.views.CustomLinkEventHandler;
import ost.newtext.views.EditText;
import ost.newtext.views.ExplicitFormField;
import ost.newtext.views.HelloWorld;
import ost.newtext.views.ImportMarkup;
import ost.newtext.views.InlineGraphic;
import ost.newtext.views.LinkedContainers;
import ost.newtext.views.MultipleColumns;
import ost.newtext.views.MultipleContainerColumns;
import ost.newtext.views.ParagraphBorder;
import ost.newtext.views.ParagraphBounds;
import ost.newtext.views.SelectText;
import ost.newtext.views.StaticHelloWorld;
import ost.newtext.views.StaticTextFlow;

[SWF(width='1080', height='720', frameRate='60')]

public class newtext extends Sprite
{
	
	public var classes:Array = [
		new ContextMenuText(),
		new CustomImportMarkup(),
		new CustomLinkEventHandler(),
		new EditText(),
		new Expl√icitFormField(),
		new HelloWorld(),
		new ImportMarkup(),
		new InlineGraphic(),
		new LinkedContainers(),
		new MultipleColumns(),
		new MultipleContainerColumns(),
		new ParagraphBorder(),
		new ParagraphBounds(),
		new SelectText(),
		new StaticHelloWorld(),
		new StaticTextFlow()
	];
	
	public function newtext() {
		super();
		stage.addEventListener( Event.ENTER_FRAME, initialize );
	}

	/**
	 * Initialize stub.
	 */
	private function initialize(event:Event):void {
		stage.removeEventListener( Event.ENTER_FRAME, initialize );
		trace( "newtext::initialize()" );
		
		//for (var i = 0; i < classes.length; i++) 
		//var thisone:Class = Class(getDefinitionByName("ost.newtext.views."+classes[Math.floor(Math.random()*classes.length)]))
		//var thisone:Class = Class(getDefinitionByName("ost.newtext.views.ParagraphBorder"))
		addChild(classes[Math.floor(Math.random()*classes.length)]);
	}
	
}

}
