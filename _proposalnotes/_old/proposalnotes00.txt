
THINGS I DON'T KNOW:

* what is going on in the plan/elevation drawing?
	* two tables, separataed by wall
	* what are the interactions?
		-+ low-latency timing requirements?
			-- i.e. when screen 1 is touched screen 2's state must change immediately to reflect this
		-+ number of users per screen
			-- number of people who can comfortably watch
				-- possibility of mirrroring interactivity on a projection or larger (cheaper) display offsides?
* what does the table look like?
	* how flexible is that look?
* how much have things been designed?
* does the budget include my fee?
	* don't want to have a conflict-of-interest while speccing hardware!
		- I am amenable to non-standard compensation schemes and the like, at this point
			+] it can be worked out I am sure, but we should sort that out with everything else
* I would benefit greatly from a rundown of the material, if it's possible to get it
	+] on my time or otherwise! whatever works.

