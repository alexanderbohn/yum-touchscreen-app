
APP DESIGN INITIAL THOUGHTS:

* Map with concentric circles blossoming under ones' fingertips
	+ (we discussed that much, yes? or am I making that up?)
	+ w/r/t AESTHETIC: see PDF in pdfscratch/_from_nikki/_world_refugees.pdf
		* visualization print map by Nikki Chung (a colleague and friend from RISD)
	+ w/r/t UX DESIGN MINUTIAE: see this Lemur demo at 8:50 in
	http://www.youtube.com/watch?v=JEjvb-yIRwY
	+ w/r/t TREATMENT OF SUBJECT:
		* two immediate possibilities: video-gamey or data-visualization-y
			+] both are ideal for overarching narrative
				-) ... vs. an open-ended toy simulation approach
			+] videogame approach must be careful not to be too literal
				* ... in evoking (say) ms. pac-man and the like 
				-) potential conflict with subject matter, in 'poor taste' etc
				+] tetris and other puzzle games are engaging and neutral
				+] DESIGN CENTERS AROUND A GAMEPLAY MECHANIC
					* ... i.e. a visual manifestation of treaty 'lateralism'
					* ... which must be fit together by the user
						* (perhaps)
					* incedental facts revealed along the way
			+] by contrast, visualization approach must not preach or become otherwise sonorous
				-) easy grooves to fall into when engaging with morally charged material
				+] story must be found to keep it afloat!
				+] DESIGN CENTERS AROUND THE REVEAL OF THE DRAMATIC ARC
					* visualization-y minutiae suggest an open-ended system
					* but actually function as constraints to move the plot along
						* i.e. seductive detail of touch animations masks use of color as axis
						* ... to convey information -- say, 'consensus temperature'
						* ... maybe that's not the best idea, specifically
							+] but the point is: one information axis hides the use of another
								* the hidden axis drives the arc reveal
		* non-narrative apps are of course doable -- the above are simplistic first-brush ideas
			+] narrative framework ensures a beginning/middle/end of a user session
				+] ... which keeps people from hogging the screen
			+] non-narrative frameworks can be used to render an introspective mood
				* as with a memorial
				* ... which is a good thing, different from preachy/sonorous
				+] ... which keeps people flowing smoothly:
				+] ... a new user takes over after overlapping alongside the previous user
	+ MOST IMPORTANTLY: NEED TO NAIL DOWN EQUIPMENT FIRST AS IT DIRECTLY AFFECTS HOW WE PLAN
		* single-user, single-touch-point systems are good for narrative-based ideas
		* multitouch tables work well with multi-user, cooperative, less deterministic ideas
			+] parallel plot threads, etc
		* so let's develop a basic idea that can be expanded as dictated by the budget!
			+] yes!

