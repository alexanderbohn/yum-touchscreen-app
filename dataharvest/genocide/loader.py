#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import re, urllib2, string, settings
from time import strptime, mktime
from datetime import date
from BeautifulSoup import BeautifulSoup, BeautifulStoneSoup, ICantBelieveItsBeautifulSoup
from django.core.management import setup_environ
setup_environ(settings)
from django.conf import settings
from django.db import models
from django.db.models import Q
from genocide.dataharvest.models import Signatory, SignatoryType

# re's
numre = re.compile('^([0-9]+)')
breakre = re.compile('([ \r\n\t\f\v]*)', re.IGNORECASE | re.MULTILINE)
sigtypere = re.compile('\s+([ad])$', re.IGNORECASE)
multibreakre = re.compile('(<br\s*\/?>(\s|&nbsp;?)*){1,}', re.IGNORECASE | re.MULTILINE)
brre = re.compile('<br\s?/?>', re.IGNORECASE)

url = 'http://www.preventgenocide.org/law/convention/UNTreatyCollection-GenocideConventionStatusReport.htm'
htmlobject = urllib2.urlopen(url)
html = unicode(htmlobject.read().decode('iso-8859-1'))
bgcolor = "e2f1ff"
dateformatter = "%d %b %Y"
signatories = []
st = {
	"a": "accession",
	"d": "succession",
	"r": "ratification",
}

def backInTime(kala):
	return date.fromtimestamp(mktime(strptime(kala, dateformatter)))

soup = BeautifulSoup(html, convertEntities=BeautifulStoneSoup.ALL_ENTITIES)

# kill sups
sups = soup.findAll('sup')
for sup in sups:
	sup.extract()

for tr in soup.findAll('table')[2].findAll('tr'):
	tds = tr('td', bgcolor=re.compile(bgcolor))
	if len(tds) > 0:
		sign = None
		tdate = None
		accd = None
		accdmatch = None
		stype = None
		if tds[0]:
			if tds[0].font:
				sign = string.replace(tds[0].font.next, "\n", "").strip()
				sign = sign.replace("\r", "")
		if tds[1]:
			if tds[1].font:
				tdate = string.replace(tds[1].font.next, "\n", "").strip()
				tdate = tdate.replace("\r", "")
		if tds[2]:
			if tds[2].font:
				accd = string.replace(tds[2].font.next, "\n", "").strip()
				accd = accd.replace("\r", "")
				accdmatch = sigtypere.search(accd)
				if accdmatch == None:
					stype = 'r' # ratification
				else:
					accd = sigtypere.sub("", accd)
					stype = accdmatch.groups()[0]
		
		# dominican republic WTF
		if (accd == None) and (stype == None) and (tdate != None):
			accd = tdate
			stype = 'r'
			tdate = None
		if (tdate != None):
			tdate = backInTime(tdate)
		if (accd != None):
			accd = backInTime(accd)
		if (stype != None):
			stype = SignatoryType.objects.filter(Q(name__contains=st[stype]))[0]

		print ">>> SIGN: %s" % sign
		print ">>> DATE: %s" % tdate
		print ">>> ACCD: %s" % accd
		print ">>> STYPE: %s" % stype
		
#		if (sign != None and accd != None):
#			dbs, dbs_created = Signatory.objects.get_or_create(
#				name=sign,
#				date=accd,
#				signatorytype=stype
#			)
#			if (tdate != None):
#				dbs.signdate = tdate
#			dbs.save()
#			if (dbs_created):
#				print ">>> New object created (id = %s)" % dbs.id
#			else:
#				print ">>> FOUND PREVIOUS OBJECT (id = %s)" % dbs.id
		print "--------------------------------------------------------------"
