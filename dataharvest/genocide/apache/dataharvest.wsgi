#!/usr/bin/env python

import sys, os
sys.stdout = sys.stderr

sys.path.append('/Users/fish/Dropbox/pureandapplied/preventgenocide-surface/dataharvest')
sys.path.append('/Users/fish/Dropbox/pureandapplied/preventgenocide-surface/dataharvest/genocide')
os.environ['DJANGO_SETTINGS_MODULE'] = 'genocide.settings'

#print ">>> sys.path: %s" % sys.path

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
