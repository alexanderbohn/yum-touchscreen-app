#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import re
import urllib2
import string
from BeautifulSoup import BeautifulSoup, BeautifulStoneSoup, ICantBelieveItsBeautifulSoup

#from pyquery import PyQuery as pq

# re's
numre = re.compile('^([0-9]+)')
breakre = re.compile('([ \r\n\t\f\v]*)', re.IGNORECASE | re.MULTILINE)
sigtypere = re.compile('\s+([ad])$', re.IGNORECASE)
multibreakre = re.compile('(<br\s*\/?>(\s|&nbsp;?)*){1,}', re.IGNORECASE | re.MULTILINE)
brre = re.compile('<br\s?/?>', re.IGNORECASE)

url = 'http://www.preventgenocide.org/law/convention/UNTreatyCollection-GenocideConventionStatusReport.htm'
htmlobject = urllib2.urlopen(url)
html = unicode(htmlobject.read().decode('iso-8859-1'))
bgcolor = "e2f1ff"
signatories = []

soup = BeautifulSoup(html, convertEntities=BeautifulStoneSoup.ALL_ENTITIES)

# kill sups
sups = soup.findAll('sup')
for sup in sups:
	sup.extract()
#fonts = soup.findAll('font')
#for font in fonts:
#	font.extract()

for tr in soup.findAll('table')[2].findAll('tr'):
	tds = tr('td', bgcolor=re.compile(bgcolor))
	if len(tds) > 0:
		sign = None
		date = None
		accd = None
		accdmatch = None
		stype = None
		if tds[0]:
			if tds[0].font:
				sign = string.replace(tds[0].font.next, "\n", "").strip()
		if tds[1]:
			if tds[1].font:
				date = string.replace(tds[1].font.next, "\n", "").strip()
		if tds[2]:
			if tds[2].font:
				accd = string.replace(tds[2].font.next, "\n", "").strip()
				accdmatch = sigtypere.search(accd)
				if accdmatch == None:
					stype = 'r' # ratification
				else:
					accd = sigtypere.sub("", accd)
					stype = accdmatch.groups()[0]
		print ">>> SIGN: %s" % sign
		print ">>> DATE: %s" % date
		print ">>> ACCD: %s" % accd
		print ">>> STYPE: %s" % stype
		print "---------------------------------------------------"
		
		
		signatories.append({
			"signatory": unicode(sign),
			"signdate": unicode(date),
			"date": unicode(accd),
			"datetype": unicode(stype)
			),
		})

#alltds = BeautifulStoneSoup(unicode(tdwad), convertEntities=BeautifulStoneSoup.ALL_ENTITIES).findAll('font')
#print ">>> %s (%s) (%s) \n" % (unicode(alltds[0].next), unicode(alltds[1].next), unicode(alltds[2].next))
#sig = string.replace(BeautifulStoneSoup(unicode(tds[0]), convertEntities=BeautifulStoneSoup.ALL_ENTITIES).td.font.next)
#sigdate = string.replace(BeautifulStoneSoup(unicode(tds[1]), convertEntities=BeautifulStoneSoup.ALL_ENTITIES).td.font.next)
#adate = string.replace(BeautifulStoneSoup(unicode(tds[2]), convertEntities=BeautifulStoneSoup.ALL_ENTITIES).td.font.next)


#for thing in soup.body.findAll('h2', text=True):
#	#print(str(thing.string))
#	morsel = breakre.sub(unicode(thing.string).strip(), "").strip()
#	#morsel = brre.sub(m, " ")
#	if (len(morsel) > 0):
#		if not numre.match(morsel):
#			print "(%s) %s" % (len(morsel), morsel)
#			print("\n--------------------------------------------------------------\n")

def whatisthisshit(morsel):
	m = morsel.find('sup').remove().end()
	mtxt = breakre.sub(unicode(m.text()), " ")
	mtxt = string.replace(mtxt, "\n", "")
	mtxt = string.replace(mtxt, "\r", "")
	#mtxt = mtxt.decode('utf-8')
	if (len(mtxt) > 0):
		if not numre.match(mtxt):
			globals()['ii'] = globals()['ii'] + 1
			signatories.append(mtxt)
			#print u"%s: %s (%s)" % (ii, unicode(mtxt), len(mtxt))
			print u"%s: %s" % (ii, unicode(mtxt))
			print u"\n--------------------------------------------------------------\n"

def parseitem(thing):
	for td in thing:
		print ">>> %s \n" % pq(td).text()

#d = pq(html, parser='html')
#d.find('h2').not_(':empty').each(whatisthisshit)
#d.find('table').eq(2).find('td[bgcolor*=e2f1ff]').find('font').each(whatisthisshit)
#d.find('table').eq(2).each(parseitem)
#d.find('table').eq(2).find('td'):
#d.find('table').eq(2).find('td[bgcolor*=e2f1ff]').find('font').each(parseitem)
#for tr in d.find('table').eq(2).filter('tr'):
#	pq(tr).each(parseitem)



print "TOTAL STUFF FOUND:"
print signatories