from django import forms
from django.conf import settings
from django.db import models
from django.template.loader import render_to_string
from django.conf import settings
from django.template import RequestContext
from django.shortcuts import render_to_response

class ColorWidget(forms.Widget):
	class Media:
		js = [
			"http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js",
			settings.ADMIN_MEDIA_PREFIX + "assets/colorpicker/js/farbtastic.js",
		]
		
	def render(self, name, value, attrs=None):
		return render_to_string("widget/color.html", locals())
		
class ColorField(models.CharField):
	def __init__(self, *args, **kwargs):
		kwargs['max_length'] = 10
		super(ColorField, self).__init__(*args, **kwargs)

	def formfield(self, **kwargs):
		kwargs['widget'] = ColorWidget
		return super(ColorField, self).formfield(**kwargs)
