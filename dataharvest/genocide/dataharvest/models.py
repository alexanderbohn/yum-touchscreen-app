import codecs
import re
from django.db import models
from django.contrib.sites.models import Site
#from genocide.dataharvest.colorpicker import ColorPickerField, ColorPickerWidget



# Create your models here.
class SignatoryType(models.Model):
	name = models.CharField(max_length=255)
	def __unicode__(self):
		return unicode(self.name.capitalize())

class Signatory(models.Model):
	name = models.CharField(max_length=255)
	signdate =  models.DateField('Signature Date', null=True, blank=True)
	date = models.DateField('R/A/S Date')
	signatorytype = models.ForeignKey(SignatoryType)
	declarations = models.TextField(blank=True)
	objections = models.TextField(blank=True)
	territory = models.TextField(blank=True)
	notes = models.TextField(blank=True)
	visible = models.BooleanField()
	offset_x = models.IntegerField('Label X Offset', blank=True, null=True)
	offset_y = models.IntegerField('Label Y Offset', blank=True, null=True)
	alias = models.CharField(max_length=255, null=True, blank=True)
	#color = ColorPickerField(null=True, blank=True)
	color = models.CharField(max_length=100, null=True, blank=True)
	color_override = models.BooleanField()
	
	def __unicode__(self):
		return "%s (%s)" % (self.name, self.date)
	
	def _getsafename(self):
		safename = self.name.replace(' ', '_').replace('\'', '_').replace(u'\xf4', 'o')
		ascname = safename.encode('ascii', 'replace')
		return unicode(ascname)
	safename = property(_getsafename)
	
	def _getoffsets(self):
		out = "None"
		if ((self.offset_x != None) and (self.offset_y != None)):
			out = "%s, %s" % (self.offset_x, self.offset_y)
		return out
	offsets = property(_getoffsets)
	
	def sanitize(self, text):
		_purell = re.compile("[\r\n\t]+", re.IGNORECASE | re.MULTILINE)
		return _purell.sub(" ", text)
	
	def _getsafedeclarations(self):
		return self.sanitize(self.declarations)
	
	def _getsafeobjections(self):
		return self.sanitize(self.objections)
	
	def _getsafeterritory(self):
		return self.sanitize(self.territory)
	
	def _getsafenotes(self):
		return self.sanitize(self.notes)
	
	safedeclarations = property(_getsafedeclarations)
	safeobjections = property(_getsafeobjections)
	safeterritory = property(_getsafeterritory)
	safenotes = property(_getsafenotes)
	
	class Meta:
		ordering = ('signdate',)
		verbose_name = "signatory"
		verbose_name_plural = "signatories"

