#!/usr/bin/env python
# encoding: utf-8
"""
admin.py

Created by FI$H 2000 on 2009-03-19.
Copyright (c) 2009 __MyCompanyName__. All rights reserved.
"""
from genocide.dataharvest.models import Signatory, SignatoryType
from django.contrib import admin
#from genocide.dataharvest.colorpicker import ColorPickerWidget

class SignatoryTypeInliner(admin.TabularInline):
	model = SignatoryType

class SignatoryTypeAdmin(admin.ModelAdmin):
	fieldsets = [
		(None, {
			'fields': [
				'name',
			]
		})
	]
	list_display = (
		'name',
		'id_col',
	)
	def id_col(self, obj):
		return obj.id
	id_col.short_description = 'Database ID'

class SignatoryAdmin(admin.ModelAdmin):
	search_fields = ['name']
	list_display = (
		'name', 'alias',
		'signdate_label',
		'date_label',
		'declarations',
		'objections',
		'territory',
		'notes',
		#'offsets_label',
		'offset_x',
		'offset_y',
		'visible',
	)
	list_display_links = ('name', 'offset_x', 'offset_y')
	list_filter = ('signatorytype','offset_x','offset_y','visible',)
	radio_fields = {'signatorytype': admin.HORIZONTAL}
	date_heirarchy = 'date'
	fieldsets = [
		(None, {'fields': [
			('name','alias','visible'),
			('offset_x','offset_y'),
			('color_override', 'color'),
		]}),
		(None, {'fields': [
			'signdate',
			('signatorytype', 'date'),
			'declarations',
			'objections',
			'territory',
			'notes',
		]})
	]
	
	def name_label(self, obj):
		if (obj.alias != None):
			return "%s (%s)" % (obj.name, obj.alias)
		else:
			return obj.name
	name_label.short_description = 'Signatory Name'
	def signdate_label(self, obj):
		if (obj.signdate != None):
			return "Sign date: %s" % obj.signdate
		else:
			return "(unsigned)"
	signdate_label.short_description = 'Signature Date'
	def date_label(self, obj):
		return "%s date: %s" % (obj.signatorytype, obj.date)
	date_label.short_description = 'R/A/S Date'
	def sigtype_label(self, obj):
		return obj.signatorytype
	sigtype_label.short_description = 'R/A/S Type'
	def offsets_label(self, obj):
		return obj.offsets
	offsets_label.short_description = "Map Label Offsets (X, Y)"
	def visible_label(self, obj):
		return obj.visible
	visible_label.short_description = "Visible on Map Display"

admin.site.register(Signatory, SignatoryAdmin)
admin.site.register(SignatoryType, SignatoryTypeAdmin)
