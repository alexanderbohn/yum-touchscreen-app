#!/usr/bin/env python
# encoding: utf-8
"""
colorpicker.py

Created by fish on 2009-11-20.
Copyright (c) 2009 __MyCompanyName__. All rights reserved.
"""

import sys
import os
import unittest
from django import forms
from django.conf import settings
from django.db import models
from django.template.loader import render_to_string
from django.conf import settings
from django.template import RequestContext
from django.shortcuts import render_to_response


class ColorPickerWidget(forms.CharField):
	class Media:
		js = [
			"http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js",
			settings.ADMIN_MEDIA_PREFIX + "assets/colorpicker/js/farbtastic.js",
		]
		
	def render(self, name, value, attrs=None):
		return render_to_string("widget/color.html", locals())
	
	def __init__(self):
		pass


class ColorPickerField(models.CharField):
	def __init__(self, *args, **kwargs):
		kwargs['max_length'] = 10
		super(ColorPickerField, self).__init__(*args, **kwargs)

	def formfield(self, **kwargs):
		kwargs['widget'] = ColorPickerWidget
		return super(ColorPickerField, self).formfield(**kwargs)




class ColorPickerTests(unittest.TestCase):
	def setUp(self):
		pass


if __name__ == '__main__':
	unittest.main()