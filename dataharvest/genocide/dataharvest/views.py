# Create your views here.
from django.template import Context, loader
from django.http import HttpResponse
from django.core import serializers
#from django.utils import simplejson
from genocide.dataharvest.models import Signatory, SignatoryType

def signatory_json_dump_all(request):
	return HttpResponse(
		serializers.serialize(
			'json',
			Signatory.objects.filter(visible=True),
			indent=4,
			excludes=(
				'visible',
				'declarations',
				'objections',
				'territory',
				'notes',
			),
			extras=(
				'__unicode__',
				'safename',
				'safedeclarations',
				'safeobjections',
				'safeterritory',
				'safenotes',
			),
			#relations=('signatorytype',)
		),
		mimetype='application/json'
	)

def signatory_json_getone_byname(request, countryname):
	return HttpResponse(
		serializers.serialize(
			'json',
			Signatory.objects.filter(name__iexact=countryname.lower()),
			indent=4,
			excludes=(
				'visible',
				'declarations',
				'objections',
				'territory',
				'notes',
			),
			extras=(
				'__unicode__',
				'safename',
				'safedeclarations',
				'safeobjections',
				'safeterritory',
				'safenotes',
			),
			#relations=('signatorytype',)
		),
		mimetype='application/json'
	)
	

def signatory_textflow_getone(request):
	s = Signatory.objects.get(name='Nonexististan') #### blahblahblah
	t = loader.get_template('textflow.xml')
	c = Context({
		"signatory_name": s.name,
		"signatory_signdates": "Signed on %s. %s on %s." % (s.signdate, s.signatorytype, s.date),
		"signatory_declarations": s.declarations,
		"signatory_objections": s.objections,
		"signatory_territory": s.territory,
		"signatory_notes": s.notes,
	})
	return HttpResponse(t.render(c))

def signatory_textflow_getone_byname(request, countryname):
	#s = Signatory.objects.get(name=countryname) #### blahblahblah
	s = Signatory.objects.get(name__iexact=countryname.lower())
	t = loader.get_template('textflow.xml')
	c = Context({
		"signatory_name": s.name,
		"signatory_signdates": "Signed on %s. %s on %s." % (s.signdate, s.signatorytype, s.date),
		"signatory_declarations": s.declarations,
		"signatory_objections": s.objections,
		"signatory_territory": s.territory,
		"signatory_notes": s.notes,
	})
	return HttpResponse(t.render(c), mimetype='application/xml')


def crossdomainxml(request):
	t = loader.get_template('crossdomain.xml')
	c = Context()
	return HttpResponse(t.render(c), mimetype='application/xml')



