from django.conf import settings
from django.conf.urls.defaults import *
from django.contrib import admin

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	# Example:
	# (r'^genocide/', include('genocide.foo.urls')),

	# Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
	# to INSTALLED_APPS to enable admin documentation:
	(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	# Uncomment the next line to enable the admin:
	(r'^admin/(.*)', admin.site.root),
	
	# signatory JSON out
	(r'^signatory/$', 'genocide.dataharvest.views.signatory_json_dump_all'),
	(r'^signatory/(?P<countryname>[\w\t ]+)/?$', 'genocide.dataharvest.views.signatory_json_getone_byname'),
	
	
	# XML out for textflow
	(r'^signatory/xml/$', 'genocide.dataharvest.views.signatory_textflow_getone'),
	(r'^signatory/xml/(?P<countryname>\w+)/?$', 'genocide.dataharvest.views.signatory_textflow_getone_byname'),
	
	# this is bad
    (r'^static/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.STATIC_DOC_ROOT}),	
	# crossdomain.xml
	(r'^crossdomain\.xml$', 'genocide.dataharvest.views.crossdomainxml'),
	
)
